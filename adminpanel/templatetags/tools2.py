from django import template
from store.helpers import handle_xp
from store.models import *


register = template.Library()

@register.simple_tag
def get_elem(dt,key):
    print(dt)
    print(key)
    return dt[key]

@register.simple_tag
def get_count_accounts(p):
    return len(p.accounts_set.all())


@register.simple_tag
def format_date(d , model = False):
    
    print(d)
    print(type(d))
    if model:
        return d.date.strftime('%a %H:%M  %d/%m/%y')
    return d.strftime('%a %H:%M  %d/%m/%y')



@register.simple_tag
def get_order(d):
    print('issueeeee here')
    return d.order

@register.simple_tag
def get_title(d):
    title = []
    detail = d.order.orderdetails_set.all()[0]
    return detail.prodname

@register.simple_tag
def get_order_title(o):
    return o.orderdetails_set.all()[0].prodname

@register.simple_tag
def get_price(d):
    price = 0
    for do in d.dorders.all():
        price += do.order_details.price * do.order_details.quantity
    return price

@register.simple_tag
def get_com_orders(s):
    return Order.objects.filter(seller = s, completed = True).count()


@register.simple_tag
def get_earned(u):
    earned = 0	
    os = Order.objects.filter(seller = u )
    for o in os:
        if o.completed:
            obj = o.orderdetails_set.all()[0]
            earned += round((obj.price * obj.quantity ) - obj.fee , 2)
    earned = round(earned,2)
    return earned


@register.simple_tag
def get_avg_rating(s):
    os = Order.objects.filter(seller = s , seller_reviewed = True)
    score = 0
    count = 0
    for o in os:
        score += o.seller_score
        count += 1

    if count == 0:
        return range(score)
    else:
        return range(score // count)


@register.simple_tag
def get_xp(s):
    total , lvl , remaining , lvlxp , max_lvl = handle_xp(s.buyer_xp , s.buyer_level)
    return [lvl , remaining , lvlxp]


@register.simple_tag
def get_replace(d):
    return d.order.orderdetails_set.all()[0].replace_time


@register.simple_tag
def get_orderdetails_childs(h):
    return h.delivery_set.all()

@register.simple_tag
def get_rev_range(n):
    return range(5-len(n))