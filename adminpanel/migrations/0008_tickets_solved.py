# Generated by Django 3.1.5 on 2021-02-11 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adminpanel', '0007_auto_20210211_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='tickets',
            name='solved',
            field=models.BooleanField(default=False),
        ),
    ]
