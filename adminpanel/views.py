from django.shortcuts import render,HttpResponse,redirect
from django.views import View
from .models import *
from .helper import *
from store.models import *
from store.helpers import req_resp,get_price,formatprod
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.

loginurl = '/admin/login/'

class Dashboard(LoginRequiredMixin,View):
	login_url = loginurl
	
	def get(self,request):
		data = req_resp(request,'Admin Dashboard')
		ps = ProductCat.objects.all()
		all_sellers = CustomUser.objects.filter(isseller = True)
		ds = Dispute.objects.filter(solved = False)
		all_users = CustomUser.objects.all()
		tot = 0
		for usr in all_users:
			tot += usr.balance
		




		if len(ps) > 4 :
			ps = ps[:4]
		if len(all_sellers ) > 4:
			sellers = all_sellers[:4]
		else:
			sellers = all_sellers

		

		data['products'] = ps
		data['sellers'] = sellers
		data['login_url'] = self.login_url
		data['sel_count'] = all_sellers.count()
		data['disp_count'] = ds.count()
		data['total_eur'] = round(tot * get_price() , 2)
		data['active'] = Order.objects.filter(status = 'active' , completed = False)
		data['completed'] =  Order.objects.filter(completed = True)
		data['disputes'] = Dispute.objects.filter(solved= False,invited = True)
		msgs = Messages.objects.filter(fromadmin = False).order_by('-date')
		if len(msgs) > 2:
			msgs = msgs[:2]

		data['msgsss'] = msgs

		
		return render(request,'dashboard.html',data)


class UsrList(LoginRequiredMixin,View):
	login_url = loginurl

	def get(self,request):
		data = req_resp(request,'User List')
		usrs = CustomUser.objects.filter().order_by('-activebalance')

		data['usrs'] = usrs
		return render(request,'userlist.html',data)


class EditUsr(LoginRequiredMixin,View):
	login_url = loginurl

	def get(self,request,usrid,errorpass =False,errorbal = False, msg = ''):
		usr = CustomUser.objects.filter(id = usrid)
		if len(usr) == 0:
			return redirect('/adminpanel/userlist')
		else:
			usr = usr[0]
		data = req_resp(request, 'Edit User - {0}'.format(usr.id))
		data['usr'] = usr
		data['errpass'] = errorpass
		data['errbal'] = errorbal
		data['msg'] = msg
		return render(request,'useredit.html' , data)

	def get_pin(self,request,base):
		pin_list = []
		for i in range(1,5):
			pin_list.append(request.POST.get(base+'{0}'.format(str(i)),None))
		if all(pin_list):
			return ''.join(pin_list)
		else:
			return False
	
	def post(self,request,usrid):
		usr = CustomUser.objects.filter(id = usrid)
		if len(usr) == 0:
			return redirect('/adminpanel/userlist')
		else:
			usr = usr[0]
			
		password = request.POST.get('pass',None)
		confpass = request.POST.get('confpass',None)
		balance = request.POST.get('balance',None)
		pin = self.get_pin(request,'addpin')

		if all([password,confpass]):
			if password == confpass:
				usr.set_password(password)
				usr.save()
				return redirect('/adminpanel/userlist')
			else:
				return self.get(request,errorpass = True , msg = 'password not matching confirmation')
		
		
		elif all([pin]):
			usr.set_pin(pin)
			usr.save()
			return redirect('/adminpanel/userlist')
		
		
		elif balance:
			try:
				balance = float(balance)
				
				diff = balance - usr.activebalance
				if diff < (-1 * usr.activebalance):
					diff = -1 * usr.activebalance
					
				usr.balance += diff
				usr.activebalance += diff
				usr.balance = round(usr.balance ,8)
				usr.activebalance = round(usr.activebalance,8)
				usr.save()
				return redirect('/adminpanel/userlist')
			except:
				return self.get(request,errorbal=True, msg ='enter a valid balance')
		else:
			return redirect('/adminpanel/userlist')




		


		


class DisputeVA(View,LoginRequiredMixin):
	login_url = loginurl
	def get(self,request,orderid,disputeid):
		
		data = req_resp(request,'Dispute Order N#{0}'.format(str(orderid)))
		

		o = Order.objects.filter(id=orderid )
		if len(o) == 0:
			
			return redirect('/adminpanel/dashboard')
		else:
			o = o[0]
		d = o.dispute_set.filter(id=disputeid)
		if len(d) == 0:
			return redirect('/adminpanel/dashboard')
		
		msgs = d[0].messages_set.all().order_by('date')
		if len(msgs) > 1000:
			msgs = msgs[:1000]
		data['dispute'] = d[0]
		data['msgs'] = msgs
		return render(request,'disputeadmin.html',data)


class ActiveSeller(LoginRequiredMixin, View):
	login_url = loginurl

	def get(self,request):
		data = req_resp(request,'Active Sellers')
		sellers = CustomUser.objects.filter(isseller = True)
		data['sellers'] = sellers
		data['login_url'] = self.login_url
		return render(request,'activeseller.html',data)

class ActiveSelling(LoginRequiredMixin, View):
	login_url = loginurl

	def get(self,request):
		data = req_resp(request,'Active Sellings')
		ps = ProductCat.objects.all()
		data['products'] = formatprod(ps)
		data['login_url'] = self.login_url
		return render(request,'activeselling.html',data)


@login_required(login_url=loginurl)
def unlist(request,prodid):
	p = ProductCat.objects.filter(productid = prodid)
	if len(p) != 0:
		p[0].admin_list = False
		p[0].save()
		return redirect('/adminpanel/dashboard')
	else:
		return HttpResponse('not found')
	
@login_required(login_url=loginurl)    
def lister(request,prodid):
	p = ProductCat.objects.filter(productid = prodid)
	if len(p) != 0:
		p[0].admin_list = True
		p[0].save()
		return redirect('/adminpanel/dashboard')
	else:
		return HttpResponse('not found')

@login_required(login_url=loginurl)
def ban(request,username):
	u = CustomUser.objects.filter(username = username)
	if len(u) != 0:
		u = u[0]
		u.banned = True
		u.save()
		return redirect('/adminpanel/dashboard')
	else:
		return HttpResponse('not found')

@login_required(login_url=loginurl)
def unban(request,username):
	u = CustomUser.objects.filter(username = username)
	if len(u) != 0:
		u = u[0]
		u.banned = False
		u.save()
		return redirect('/adminpanel/dashboard')
	else:
		return HttpResponse('not found')



class TicketsPanel(LoginRequiredMixin, View):
	
	login_url = loginurl

	def get(self,request):
		data = req_resp(request, 'Tickets')
		ts = Tickets.objects.filter(solved = False).order_by('date')
		data['tickets']= ts
		return render(request,'tickets.html',data)

@login_required(login_url=loginurl)
def solveticket(request,ticketid):
	t = Tickets.objects.filter(id= int(ticketid) , solved = False)
	if len(t) > 0:
		t = t[0]
		t.solved = True
		t.save()
		return redirect('/adminpanel/tickets')
	else:
		return redirect('/adminpanel/tickets')



class AdminLogin(View):

	def get(self,request):
		pass
		
