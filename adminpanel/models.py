from django.db import models
from store.tools import encrypt_str
from store.models import CustomUser

# Create your models here.


class AdminUser(models.Model):
    email = models.CharField(max_length=255,unique = True)
    username =  models.CharField(max_length=255,unique=True)
    password = models.CharField(max_length=255)


    def set_password(self,password):
        self.password = encrypt_str(password)
    
    def confirm_password(self,password):
        if self.password == encrypt_str(password):
            return True
        else:
            return False

    def __str__(self):
        return self.email+' '+self.username



class Settings(models.Model):
    dispute_hours = models.IntegerField(default=0)
    upgrade_price_seller = models.FloatField(default=0)
    upgrade_price_buyer = models.FloatField(default=0)
    upgrade_to_seller = models.FloatField(default = 0)
    auction_duration = models.FloatField(default=0)
    restart_auction_days = models.IntegerField(default=0)
    stick_price = models.FloatField(default=0)
    btc_price = models.FloatField(default=0)
    lvl_step = models.IntegerField(default=0)
    

class LevelManager(models.Model):
    lvl = models.IntegerField(default=1)
    buyer_xp = models.IntegerField(default=0)
    seller_xp = models.IntegerField(default=0)
    buyer_trade_fee = models.FloatField(default=0)
    seller_trade_fee = models.FloatField(default=0)
    withdraw_fee = models.FloatField(default=0)
    deposit_fee = models.FloatField(default=0)
    feature_price = models.FloatField(default=0)

    def __str__(self):
        return str(self.lvl)




class Tickets(models.Model):
    user = models.ForeignKey(CustomUser, on_delete = models.CASCADE)
    title = models.CharField(max_length=255)
    details = models.TextField()
    #file = models.FileField()
    date = models.DateTimeField(auto_now_add=True)
    solved = models.BooleanField(default = False)

    def __str__(self):
        return self.title 


