import requests as req
import json
from marketplace.api_sett import *
import time




headers = {'content-type': 'application/json'}
payload = {"jsonrpc": "1.0", "id":"curltest", "method": "", "params": [] }
payload_2 = {"jsonrpc":"2.0","id":"0","method":"","params":{}}




class Bitcoin:
    def __init__(self):
        self.user = 'tester'
        self.password = 'tester123'
        self.url = 'http://{0}:{1}@103.155.93.196:18332/'.format(self.user,self.password)
        #self.user = 'exchanger'  103.155.93.196
        #self.password = 'exchangerpass'
        #self.url = 'http://{0}:{1}@23.254.176.26:8332/'.format(self.user,self.password)








    def send(self,command,*args,aray=None,message=False):
        if len(args) != 0:
            params = [x for x in args]
            if aray:
                params.append(aray)
            else:
                pass
        else:
            params = []

        custom_payload = payload.copy()
        custom_payload['method'] = command
        custom_payload['params'] = params
        #print(custom_payload)
        # sending and recieving response from the node
        while True:
            try:
                resp = req.post(self.url,json = custom_payload,headers = headers)
                break
            except Exception as e:
                print('request failed')
                print(str(e))
                time.sleep(5)
        data = json.loads(resp.content.decode())
        #print(data)
        if data['error']:
            print('bitcoin node error')
            print(data['error'])
            if message:
                return [False,data['error']['message']]
            else:
                return False
        else:
            if message:
                return [data['result']]
            else:
                return data['result']


class Monero:
    def __init__(self):
        #self.user  = monero_user
        #self.password = monero_password
        #self.url = 'http://127.0.0.1:18083/json_rpc'.format(self.user,self.password)
        self.url = 'http://{0}:{1}/json_rpc'.format(m_ip,m_port)
    def send(self,command,**kwargs):
        params = dict(kwargs)
        if 'in_' in params.keys():
            del params['in_']
            params['in'] = True

        #print(params)
        custom_payload = payload_2.copy()
        custom_payload['method'] = command
        custom_payload['params'] = params
        #print(custom_payload)  ,auth=HTTPDigestAuth(self.user,self.password)
        while True:
            try:
                resp = req.post(self.url , json=custom_payload,headers=headers)
                break
            except:
                print('request failed')
                time.sleep(5)
        #print(resp.content.decode())
        data = json.loads(resp.content.decode())
        #print(data)
        if 'error' in data.keys():
            print(data)
            print(data['error'])
            return False

        return data



def get_handler(coin):
    if coin == 'BTC':
        return Bitcoin()
    elif coin == 'XMR':
        return Monero()
    else:
        return False
