from __future__ import absolute_import, unicode_literals

import os

from celery import Celery
#from store.tasks import count_users




# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'marketplace.settings')

app = Celery('marketplace')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'balance_checker':{
        'task':'store.tasks.check_deposit',
        'schedule' : 30.0,
    },
    'order_checker' : {
        'task' : 'store.tasks.check_active_orders',
        'schedule': 15.0,
    },
    'auction_starter' : {
        'task' : 'store.tasks.check_auction',
        'schedule' : 20.0,

    },
    'auction_checker' : {
        'task' : 'store.tasks.check_winner',
        'schedule' : 30.0,
    },
    'review_checker' : {
        'task' : 'store.tasks.check_review',
        'schedule' : 30.0,
    },
    'price_updater' : {
        'task' : 'store.tasks.price_updater',
        'schedule' : 30.0,
    }
} 

# Load task modules from all registered Django app configs.


#


""" @app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 10 seconds.
    sender.add_periodic_task(10.0, count_users(), name='add every 10') """





@app.task(bind=True)
def debug_task(self,*args):
    print('Request: {0!r}'.format(self.request))
