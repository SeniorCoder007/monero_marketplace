"""marketplace URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include
from django.conf import settings
from django.conf.urls.static import static
from adminpanel.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    # admin panel
    path('adminpanel/dashboard',Dashboard.as_view()),
    path('adminpanel/activesellers',ActiveSeller.as_view()),
    path('adminpanel/activesellings',ActiveSelling.as_view()),
    path('adminpanel/unlist/<str:prodid>',unlist),
    path('adminpanel/list/<str:prodid>',lister),
    path('adminpanel/unban/<str:username>',unban),
    path('adminpanel/ban/<str:username>',ban),
    path('adminpanel/tickets',TicketsPanel.as_view()),
    path('adminpanel/markassolved/<int:ticketid>',solveticket),
    path('adminpanel/dispute/<int:orderid>/<int:disputeid>',DisputeVA.as_view()),
    path('adminpanel/userlist',UsrList.as_view()),
    path('adminpanel/useredit/<int:usrid>',EditUsr.as_view()),

    ###
    path('',include('store.urls')),   
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL , document_root= settings.MEDIA_ROOT)
print(include('adminpanel.urls'))