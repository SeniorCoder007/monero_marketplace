import requests as req
import json



class Ticker:
    def __init__(self):
        api_key = 'f621b765-8c32-4312-b744-826781a328c9'
        self.base_url = 'https://api.blockchain.com/v3/exchange/'
        self.headers = {
            'X-API-Token':api_key,
            'accept': 'application/json'
        }

    
    def get_price(self,symbol="BTC-EUR"):
        try:
            res = req.get(self.base_url+'tickers/'+symbol,headers= self.headers)
            #print(res.content.decode())
            d = json.loads(res.content.decode())
            #print(d)
            return d['last_trade_price']
        except Exception as e:
            return False