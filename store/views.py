from os import error
from django.shortcuts import render,HttpResponse,redirect
from django.views import View
from .models import *
from .helpers import get_sel_orders, get_avg_rating,get_earned,calculate_fee,get_total,get_price,handle_xp , format_sec,get_date,get_product,format_cart,generate_addr,formatsingleprod,isLogged,req_resp,check_username,get_user,getProduct,getProductUser,formatprod
from uuid import uuid4
from string import ascii_letters, ascii_lowercase, digits
import random
from .tools import verify_str
from .serializer import *
from marketplace.settings import dispute_hours,level_step,mon_index
from .tasks import handle_withdraw
from marketplace.node_handler import get_handler
from adminpanel.models import *
import datetime
from django.core.files.storage import FileSystemStorage

# Create your views here.



def index(request):
	return redirect('/products')



# Auth Section



class Login(View):

	def get(self,request,error=False,error_msg=''):
		if isLogged(request):
			print('here')
			return redirect('/products')
		data = {
		'title':'Login',
		'error': error,
		'error_msg' : error_msg
		}
		return render(request, 'login.html',data)
	
	def get_user(self,email):
		u = CustomUser.objects.filter(email=email)
		if len(u) == 0:
			return False
		else:
			return u[0]
		

	def post(self,request):
		username  = request.POST.get('username',None)
		password = request.POST.get('password',None)
		if all([username,password]):
			u = check_username(username)
			if u:
				if u.banned:
					return self.get(request,error=True,error_msg='You are banned')
				if u.check_password(password):
					print('logged in')
					request.session['logged'] = True
					request.session['email'] = u.email
					return redirect('/products')
				else:
					return self.get(request,error=True,error_msg='Password is wrong')

			else:
				return self.get(request,error=True,error_msg='Account not found')
		else:
			return self.get(request,error=True,error_msg='Please submit your credentials first.')


		




class Register(View):

	def get(self,request,error = False,error_msg='',name=None,email=None):
		if isLogged(request):
			return redirect('/products')
		data = {
		'title':'Register',
		'error' : error,
		'error_msg' : error_msg,
		'name' : name,
		'email' : email
			}		
		return render(request, 'register.html',data)


	def is_user(self,email):
		u = CustomUser.objects.filter(email=email)
		if len(u) == 0 :
			return False
		else:
			return True
	
	def extra_checks(self,email,password,username):
		if email:
			if ' ' in username or verify_str(username):
				return 'username'
			else:
				if len(list(filter(str.isdigit,password))) < 6 or len(list(filter(str.isalnum,password))) == 0 or len(list(filter(str.isupper,password))) == 0:
					return 'numbers'
				else:
					return True
		else:
			return 'email'


	
				

	
	def post(self,request):
		name = request.POST.get('name',None)
		email  = request.POST.get('email',None)
		#confirmation = request.POST.get('confirm',None)
		password = request.POST.get('password',None)
		confpassword = request.POST.get('confirmpassword',None)
		

		pin_list = []
		for i in range(1,5):
			pin_list.append(request.POST.get('pin{0}'.format(str(i)),None))

		if all(pin_list):
			pin = ''.join(pin_list)
		else:
			return 	self.get(request,error=True,error_msg='Empty pin fields. Please Verify',name=name,email=email)

		pin_list = []
		for i in range(1,5):
			pin_list.append(request.POST.get('confirmpin{0}'.format(str(i)),None))

		if all(pin_list):
			confpin = ''.join(pin_list)
		else:
			return 	self.get(request,error=True,error_msg='Empty confirm pin fields. Please Verify',name=name,email=email)
		
		if all([name,email,password,confpassword]):
			if not self.is_user(email):
				res = self.extra_checks(email,password,name)
				if res == 'email':
					return self.get(request,error=True,error_msg='Emails not matched')
				elif res == 'username':
					return self.get(request,error=True,error_msg='Username contain spaces or special characters')
				elif res == 'numbers':
					return self.get(request,error=True,error_msg='password must contain 6 digits or more, special and uppercase characters',name=name,email=email)
				
				if password != confpassword:
					return self.get(request,error=True,error_msg='password missmatch. Please verify',name=name,email=email)
				
				if pin != confpin:
					return self.get(request,error=True,error_msg='pin missmatch. Please verify',name=name,email=email)
				
				print('creating')
				addr = generate_addr()
				u = CustomUser(username=name,email=email,pin=int(pin))
				u.set_pin(pin)
				u.set_password(password)
				#u.btcdeposit = addr
				n = get_handler('XMR')
				resp = n.send('create_address',account_index=mon_index)
				u.address = resp['result']['address']
				u.save()
				request.session['logged'] = True
				request.session['email'] = email
				return redirect('/login')
			else:
				return self.get(request,error=True,error_msg='email address is already used.',name=name,email=email)
		else:
			return self.get(request,error=True,error_msg='A field is invalid please check again.',name=name,email=email)




def logout(request):
	print('here')
	del request.session['logged']
	del request.session['email']
	return redirect('/login')


# submit ticket

class SubmitTicket(View):


	def post(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		title = request.POST.get('title',None)
		details = request.POST.get('details',None)
		#file = request.FILES.get('file',None)
		print(request.POST)
		#print(request.FILES)
		if all([title,details]):
			fs = FileSystemStorage()
			#name = fs.save(file.name, file)
			#url = fs.url(name)
			#print(url)
			t = Tickets.objects.create(user = u,title = title , details = details)#, file = name)
			t.save()
			return support(request,show=True, success =True, msg='ticket created successfuly')
		else:
			return support(request,show=True,success=False,msg='Please check fields again')
		




		


	


#########


# Buyer/User View 

def get_user_tickets(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')

	u = get_user(request.session['email'])
	tickets = Tickets.objects.filter(user = u).order_by('-date')
	data = req_resp(request,'My Tickets')
	data['tickets'] = tickets
	return render(request,'user/usertickets.html',data)


class Dashboard(View):

	def count_disp(self,u):
		os = u.order_set.all()
		count = 0
		for o in os:
			if len(o.dispute_set.all()) != 0:
				count += 1
		
		return count

	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Buyer Dashboard')
		u = get_user(request.session['email'])
		trs_deposit  = u.transactions_set.filter(tr_type='deposit').order_by('-date')
		trs_withdraw = u.transactions_set.filter(tr_type='withdraw').order_by('-date')
		data['deposit_trs'] = trs_deposit
		data['withdraw_trs'] = trs_withdraw
		completed = []
		active = []
		ds = []
		os = Order.objects.filter(buyer = u )
		for o in os:
			if o.completed:
				completed.append(o)
			elif o.status == 'active':
				active.append(o)
			if o.dispute_set.filter(solved = False).count() > 0:
				ds.append(o.dispute_set.filter(solved = False)[0])
			
		total , lvl , remaining , lvlxp , max_lvl = handle_xp(u.buyer_xp , u.buyer_level)
		s = Settings.objects.all()[0]
		data['up_cost'] = s.upgrade_price_buyer
		data['upgradable'] = u.buyer_upgradable
		data['upgrade_to_seller_cost'] = s.upgrade_to_seller
		data['lvl'] = lvl
		data['rem'] = remaining
		data['lvlxp'] = lvlxp
		
		data['completed'] = completed
		data['active'] = active
		data['disputed'] = ds
		data['com_count'] = u.orderbuyer.filter(completed = True).count()
		data['spent'] = round(get_total(u.orderbuyer.all()) , 8) 
		data['isseller'] = u.isseller
		try:
			
			data['com_rate'] = round((u.orderbuyer.filter(completed = True).count() / u.orderbuyer.all().count()) * 100 , 2)
			data['act_rate'] = round((u.orderbuyer.filter(status = 'active').count() / u.orderbuyer.all().count()) * 100,2)
			data['disp_rate'] = round((len(ds) / u.orderbuyer.all().count()) * 100 , 2)
		except ZeroDivisionError:
			data['com_rate'] = 0
			data['act_rate'] = 0
			data['disp_rate'] = 0
		
		
		
		return render(request,'user/dashboardbuyer.html',data)


def balance_error(request,cost=0,error=False):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	data = req_resp(request,'Low Balance')
	if not error:
		return redirect('/user/dashboard')
	else:
		data['am_eur'] = cost
		data['am_btc'] = round(cost/ get_price() , 8)
		return render(request,'user/lowbalance.html',data)
def upgrade_to_seller(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	s = Settings.objects.all()[0]
	up_price = round(s.upgrade_to_seller / get_price() , 8) 
	if not u.isseller and u.activebalance >= up_price:
		u.isseller = True
		u.activebalance -= up_price
		u.balance -= up_price
		u.balance = round(u.balance ,8)
		u.activebalance = round(u.activebalance,8)
		u.save()
	elif u.activebalance < up_price:
		return balance_error(request,cost =s.upgrade_to_seller , error = True )


	return redirect('/seller/dashboard')




def upgrade_buyer_lvl(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	s = Settings.objects.all()[0]
	total , lvl , remaining , lvlxp , new_lvl = handle_xp(u.buyer_xp , u.buyer_level)
	up_price = round(s.upgrade_price_buyer / get_price() , 8) 
	if remaining == 0 and u.buyer_upgradable and u.activebalance >= up_price :
		u.buyer_level += 1
		u.buyer_upgradable = False
		u.activebalance -= up_price
		u.balance -= up_price
		u.balance = round(u.balance ,8)
		u.activebalance = round(u.activebalance,8)
		u.save()
	elif u.activebalance < up_price:
		return balance_error(request,cost =s.upgrade_price_buyer , error = True )

	return redirect('/user/dashboard')





def myorders(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	data = req_resp(request,'My Orders')
	u = get_user(request.session['email'])
	os = u.orderbuyer.all()
	dt = []
	for o in os:
		for od in o.orderdetails_set.all():
			s = SellingSerializer(od).data
			s['seller'] = o.seller
			s['ordid'] = o.id

			dt.append(s)
		

	#print(s)
		
	data['sellinngs'] =  dt
	return render(request, 'user/myorders.html',data)




class UserProfile(View):

	def get(self,request,btcaddrerror=False,addpinerror=False,pinerr=False , emailerr=False,passerr = False,passmsg = ''):
		if not isLogged(request):
			print('here')
			return redirect('/products')

		u = get_user(request.session['email'])


		data = req_resp(request,'User Settings')
		s = Settings.objects.all()[0]
		data['upgrade_to_seller_cost'] = s.upgrade_to_seller
		data['btcaddress'] = u.btcwithdraw
		data['isseller'] = u.isseller
		if btcaddrerror:
			data['btcerror'] = True
		if addpinerror:
			data['pinaddrerror'] = True
		data['emailerror'] = emailerr
		data['passerr'] = passerr
		data['pinerr'] = pinerr
		data['passmsg'] = passmsg

		print(data)
		return render(request,'user/profile.html',data)


	def get_pin(self,request,base):
		pin_list = []
		for i in range(1,5):
			pin_list.append(request.POST.get(base+'{0}'.format(str(i)),None))
		if all(pin_list):
			return ''.join(pin_list)
		else:
			return False

	def check_pass(self,password):
		if len(list(filter(str.isdigit,password))) < 6:
			return False
		else:
			return True


	def post(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		#username = request.POST.get('username',None)
		email = request.POST.get('email',None)
		oldpass = request.POST.get('oldpass',None)
		newpass = request.POST.get('newpass',None)
		confirmnewpass = request.POST.get('confirmnewpass',None)
		oldpin = self.get_pin(request,'oldpin')
		newpin = self.get_pin(request,'newpin')
		print('@@@@@@@@@@@@')
		print(oldpin)
		print(newpin)
		addpin = self.get_pin(request,'addpin')
		btcdeposit = request.POST.get('btcdeposit',None)
		if all([email]):
			u = get_user(request.session['email'])
			if u:
				""" if not  u.username == username:
					u2 = check_username(username)
					if u2:
						return HttpResponse('Username is already taken')
					else:
						u.username = username
						u.save()
						request.session['username'] = username """
						
				if not u.email == email:
					u2 = get_user(email)
					if u2:
						return self.get(request,emailerr=True)
					else:
						u.email = email
						u.save()
						request.session['email'] = email
				
				return redirect('/user/settings')

			else:
				return redirect('/login')

		elif all([oldpass,newpass,confirmnewpass]):
			u = get_user(request.session['email'])
			if u:
				if u.check_password(oldpass):
					if newpass == confirmnewpass:
						if self.check_pass(newpass):
							u.set_password(newpass)
							u.save()
							return redirect('/user/settings')
						else:
							return self.get(request,passerr=True,passmsg = 'password not respecting criteria')
					else:
						return self.get(request,passerr=True,passmsg = 'Password Mismatch')
				else:
					return self.get(request,passerr=True,passmsg = 'Wrong password')
			else:
				return redirect('/login')

		elif all([oldpin,newpin]):
			u = get_user(request.session['email'])
			if u:
				if u.confirm_pin(oldpin):
					u.set_pin(newpin)
					u.save()
					return redirect('/user/settings')
				else:
					return self.get(request,pinerr = True)
				
			else:
				return redirect('/login')

		elif all([btcdeposit,addpin]):
			u = get_user(request.session['email'])
			if u:
				if len(btcdeposit) == 0:
					return self.get(request,btcaddrerror=True)
				if u.confirm_pin(addpin):
					h = get_handler('XMR')
					res = h.send('validate_address', address=btcdeposit)
					if res['result']['valid']:
						u.xmrwithdraw = btcdeposit
						u.save()
						return redirect('/user/settings')
					else:
						return self.get(request,btcaddrerror=True)
				else:
					return self.get(request,addpinerror=True)
				
			else:
				return redirect('/login')

		else:
			return redirect('/user/settings')


class Deposit(View):
	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		data = req_resp(request,'Deposit')
		data['btc_deposit_addr'] = u.address
		data['balace'] = u.balance
		print(data)

		return render(request,'user/deposit.html',data)

class Withdraw(View):

	def get_pin(self,request,base):
		pin_list = []
		for i in range(1,5):
			pin_list.append(request.POST.get(base+'{0}'.format(str(i)),None))
		if all(pin_list):
			return ''.join(pin_list)
		else:
			return False

	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Withdraw')
		u = get_user(request.session['email'])
		data['address'] = u.btcwithdraw
		return render(request,'user/withdraw.html',data)

	def post(self,request):
		u = get_user(request.session['email'])
		amount = request.POST.get('amount',False)
		pin = self.get_pin(request,'withdrawpin')
		if all([amount,pin]):
			if u.confirm_pin(pin):
				try:
					fees = calculate_fee(float(amount) , u.buyer_level)
					amount = amount - fees 
					amount_btc = round(float(amount) / get_price() , 8)
				except:
					return HttpResponse('Something is wrong')
				if amount_btc > u.activebalance:
					return HttpResponse('not enough balance')
				else:
					tr = u.transactions_set.create(address = u.btcwithdraw,amount = amount_btc,tr_type = 'withdraw', fee = fees)
					tr.save()
					u.balance -= amount_btc
					u.activebalance -= amount_btc
					u.placed_for_withdraw += amount_btc
					u.balance = round(u.balance,8) #- (10**-8)
					u.activebalance = round(u.activebalance,8)  #- (10**-8)
					u.save()
					handle_withdraw.delay(tr.id)
					return redirect('/user/withdraw')


			else:
				return HttpResponse('Wrong pin')
		pass
class Balance(View):
	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Balance')
		return render(request,'user/balance.html',data)

# cart mini section

def cart(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	data = req_resp(request,'Cart')
	return render(request, 'cart.html',data)

class Cartview(View):

	


	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Cart')
		cartobjs = get_user(request.session['email']).cart_set.all()
		final , total = format_cart(cartobjs)
		data['cart'] = final
		data['total'] = total
		return render(request, 'cart.html',data)

	def post(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		cartobjs = get_user(request.session['email']).cart_set.all()
		for i in range(len(cartobjs)):
			idd = request.POST.get('prodid{0}'.format(str(i)),None)
			quantity = request.POST.get('quantity{0}'.format(str(i)),None)
			if all([idd,quantity]):
				c = Cart.objects.filter(id = int(idd))
				if len(c) == 0:
					continue
				else:
					c = c[0]
					if int(quantity) <= ProductCat.objects.filter(productid=c.productid)[0].accounts_set.all().count():
						c.quantity = int(quantity)
					else:
						c.quantity =  ProductCat.objects.filter(productid=c.productid)[0].accounts_set.all().count()

					c.save()
			
		return redirect('/user/cart')

class BuyProduct(View):
	def post(self,request,prodid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		carts = u.cart_set.all()
		for cart in carts:
			cart.delete()
		quantity = request.POST.get('quantity',None)
		if quantity:
			try:
				quantity = int(quantity)
				if quantity <= ProductCat.objects.filter(productid = prodid)[0].accounts_set.all().count():
					c = u.cart_set.create(productid =ProductCat.objects.filter(productid = prodid)[0] ,quantity = quantity)
					c.save()
					return redirect('/user/checkoutlist')
				else:
					return redirect('/products')
			except Exception as e:
				print(str(e))
				return redirect('/products')
		else:
			return redirect('/products')





def addtocart(request, prodid):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	direct = request.GET.get('direct',False)
	u = get_user(request.session['email'])
	res = u.cart_set.filter(productid = prodid)
	if len(res) != 0:
		res = res[0]
		if not res.quantity +1 > ProductCat.objects.filter(productid=prodid)[0].accounts_set.all().count():
			
			res.quantity += 1
		res.save()
	else:
		u.cart_set.create(productid = prodid)
	if direct:
		return redirect('/user/paymentdetails')
	else:
		return redirect('/products')

def checkoutlist(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	data = req_resp(request,'Checkout List')
	cartobjs = get_user(request.session['email']).cart_set.all()
	final , total = format_cart(cartobjs)
	data['cart'] = final
	data['total'] = total
	return render(request, 'checkoutlist.html',data)


def deletecartelem(request,idd):
	if not isLogged(request):
			print('here')
			return redirect('/products')
	u = get_user(request.session['email'])
	c = u.cart_set.filter(id = idd )
	if not  len(c) == 0:
		c[0].delete()
	return redirect('/user/cart')



def paymentdetails(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	data = req_resp(request,'Payment Details')
	return render(request, 'paymentdetails.html',data)


class Paymentdetails(View):


	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Payment Details')
		u = get_user(request.session['email'])
		cartobjs = get_user(request.session['email']).cart_set.all()
		final , total = format_cart(cartobjs)
		fees = calculate_fee(total,u.buyer_level)
		data['cart'] = final
		data['total'] = total
		data['converted'] = round( total / get_price() , 8)
		data['fees'] = fees
		data['converted_fees'] = round( fees / get_price() , 8)
		return render(request, 'paymentdetails.html',data)

	def post(self,request):
		cartobjs = get_user(request.session['email']).cart_set.all()
		final , total = format_cart(cartobjs)
		u = get_user(request.session['email'])
		fees = calculate_fee(total,u.buyer_level)
		if u.activebalance * get_price() < (total + fees):
			return balance_error(request,cost=(total + fees),error=True)

		

		seller = check_username(final[0]['seller'])
		#o = u.order_set.create(price = total, status = 'active', fee = fees)
		o = Order.objects.create(buyer = u , total = total , buyer_fee = fees , seller = seller   )
		
		act_tracker = []
		if len(final) == 0:
			o.delete()
			return redirect('/products')
		obj = final[0]
		prod = ProductCat.objects.filter(productid = obj['prodid'])
		if len(prod) > 0:
			prod = prod[0]
		else:
			act_tracker.append(False)
			cartobjs[0].delete()
			

			



			
			

			
		fee = calculate_fee(obj['price'] * obj['quantity'] , seller.seller_level , buyer=False )
		od = o.orderdetails_set.create(prodname = obj['name'],price = obj['price'],quantity = int(obj['quantity']), replace_time = int(obj['replace_time']),fee = fee)#,status='active')
			
			
		for _ in range(obj['quantity']):
			accs = prod.accounts_set.all()
			if len(accs) > 0:
				acc = accs[0]
			else:
				act_tracker.append(False)
				obj.delete()
				continue

			content = ""
				
			cont = acc.line.split(',')
				
				
			for i in range(len(cont)):
				content += '{0}\n'.format(cont[i])
						

			deliv = od.delivery_set.create(content=content)
			deliv.save()
			act_tracker.append(True)
			acc.delete()
		if all(act_tracker):
			for obj in cartobjs:
				obj.delete()
				print('cleared')
			u.balance -=  ((total + fees) / get_price())
			u.activebalance -= ((total + fees) / get_price())
			u.balance = round(u.balance ,8)
			u.activebalance = round(u.activebalance,8)
			notif = Notifications.objects.create(user= u,content = 'Started Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			notif = Notifications.objects.create(user= seller,content = 'Started Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			o.save()
			
			od.save()
			u.save()
			return redirect('/user/orderdetail/'+str(o.id))
		else:
			o.delete()
			return redirect('/products')
			


	

class OrderDetail(View):

	def get(self,request,orderid,feedbackerror = False):
		#input(orderid)
		print('### jere ')
		print(len(request.user.username ))
		if not isLogged(request) and len(request.user.username ) == 0:
			print('here')
			return redirect('/products')
		
		if len(request.user.username ) == 0:
			u = get_user(request.session['email'])
			
			o = u.orderbuyer.filter(id=orderid )
			if len(o) == 0:
				admin = False
				print('not found')
				""" ods = OrderHolder.objects.filter(seller = u.username)
				if len(ods) == 0:
					return redirect('/products')
				else:
					for od in ods:
						if od.order.id == orderid:
							o = od.order
							holders = o.orderholder_set.all()
							break
					else: """
				o = u.orderseller.filter(id=orderid )
				if len(o) != 0:
					o = o[0]
					""" notifs = Notifications.objects.filter(user = u , url = '/user/orderdetail/'+str(o.id))
					for notif in notifs:
						notif.read = True
						notif.save() """
				else:	
					
					return redirect('/products')
			else:
				admin = False
				o = o[0]
				""" notifs = Notifications.objects.filter(user = u , url = '/user/orderdetail/'+str(o.id))
				for notif in notifs:
					notif.read = True
					notif.save() """
		else:
			o = Order.objects.filter(id = orderid)
			if len(o) == 0:
				print('zerooooooo')
				return redirect('/login')
			else:
				admin = True
				o = o[0]			

		#o = Order.objects.filter(id = orderid)
		
			
		data = req_resp(request,'Order#{0} - Details'.format(str(orderid)))
		if o.buyer_reviewed:
			data['reviewed'] = True


		details = o.orderdetails_set.all()
		
		data['order'] = o
		data['details'] = details

		dispute_hours = details[0].replace_time
		hour , minute , second = get_date(o.date)
		s = Settings.objects.all()[0]
		if (hour * 3600 + minute * 60 + second ) > dispute_hours * 60:
			hour , minute ,second = 0,0,0
		else:
			hour, minute , second = format_sec(dispute_hours * 60 - (hour * 3600 + minute * 60 + second ) )

		if o.completed:
			hour , minute , second = 0,0,0

		data['timeleft'] = '{0}:{1}:{2}'.format(hour,minute,second)
		
		data['pre_total'] = o.total
		
		
		data['feedbackerror'] = feedbackerror

		print(data)


		if admin:
			return render(request, 'orderdetailsadmin.html' , data)
		else:
			return render(request,'user/orderdetails.html',data)

	def post(self,request,orderid):
		idd = orderid
		if idd:
			u = get_user(request.session['email'])
			o = u.orderbuyer.filter(id=idd)
			if len(o) == 0:
				return redirect('/user/orderdetail/'+str(idd))
			else:
				o = o[0]
			notif = Notifications.objects.create(user= u,content = 'Completed Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			o.released = True
			
			
			details = o.orderdetails_set.all()
			total = 0
			for detail in details:
				total += ((detail.price * detail.quantity) - detail.fee)
			seller = o.seller
			notif = Notifications.objects.create(user= seller,content = 'Completed Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			seller.balance += (total / get_price())
			seller.activebalance += (total / get_price())
			seller.balance = round(seller.balance ,8)
			seller.activebalance = round(seller.activebalance,8)
			o.buyer_reviewed = True
			rate = int(o.total*0.1)
			b = o.buyer
			total , lvl , remaining , lvlxp , new_lvl = handle_xp(b.buyer_xp , b.buyer_level,rate)
			print('here #####@@@@@@@@')
			print(total)
			print(lvl)
			print(remaining)
			print(lvlxp)
			print(new_lvl)
			print(rate)
			print('@@@@@@@@@@@')
			b.buyer_xp = total
			if new_lvl > b.buyer_level:
				b.buyer_upgradable = True
			""" if int(rating) < 3:
				b.buyer_negative += 1
			else:
				b.buyer_positive += 1 """
			b.save()
			seller.save()
			o.completed = True
			o.status = 'completed'
			o.save()
			return  redirect('/user/orderdetail/'+str(idd))




		else:
			return redirect('/user/orderdetail/'+str(idd))


####


### Order Dispute ###

class StartDispute(View):

	def get(self,request,orderid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Order#{0} - Start Dispute'.format(str(orderid)))
		u = get_user(request.session['email'])
		o = u.orderbuyer.filter(id=orderid )
		#o = Order.objects.filter(id = orderid)
		if len(o) == 0:
			""" ods = OrderHolder.objects.filter(seller = u.username)
			if len(ods) == 0:
				return redirect('/products')
			else:
				for od in ods:
					if od.order.id == orderid:
						o = od.order
						holders = o.orderholder_set.all()
						break
				else:
					return redirect('/products') """
			return redirect('/products')
		else:
			o = o[0]
			

		
		d = o.dispute_set.create()
		d.save()

		notif = Notifications.objects.create(user= o.buyer,content = 'Opened Dispute for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
		notif.save()
		notif = Notifications.objects.create(user= o.seller,content = 'Opened Dispute for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
		notif.save()

		o.completed = False
		o.status = 'pending'
		o.save()

		return redirect('/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
	
	def post(self,request,orderid):
		if not isLogged(request):
			print('here')
			return redirect('/products')

		# check if valid time 

		return self.get(request,orderid)


class DisputeV(View):
	def get(self,request,orderid,disputeid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Dispute Order N#{0}'.format(str(orderid)))
		u = get_user(request.session['email'])
		

		o = u.orderbuyer.filter(id=orderid )
		if len(o) == 0:
			o = u.orderseller.filter(id=orderid )
			if len(o) != 0:
				o = o[0]
				""" notifs = Notifications.objects.filter(user = u , url = '/user/dispute/{0}/{1}'.format(str(orderid),str(disputeid)))
				for notif in notifs:
					notif.read = True
					notif.save() """

			else:	
				return redirect('/products')
		else:
			o = o[0]
			""" notifs = Notifications.objects.filter(user = u , url = '/user/dispute/{0}/{1}'.format(str(orderid),str(disputeid)))
			for notif in notifs:
				notif.read = True
				notif.save() """
		d = o.dispute_set.filter(id=disputeid)
		if len(d) == 0:
			return redirect('/products')
		
		msgs = d[0].messages_set.all().order_by('date')
		if len(msgs) > 1000:
			msgs = msgs[:1000]
		data['dispute'] = d[0]
		data['msgs'] = msgs
		return render(request,'user/dispute.html',data)
	
	def post(self,request,orderid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Order#{0} - Start Dispute'.format(str(orderid)))
		u = get_user(request.session['email'])
		o = u.order_set.filter(id=orderid )[0]
		
		o.completed = False
		o.status = 'pending'
		o.save()
		print(request.POST)
		if o.user.username != u.username:
			return redirect('/products')
		for k in dict(request.POST).keys():
			print('here')
			if 'choice' in k:
				k_id = k[-1]
			else:
				continue
			print(len(request.POST[k]))
			for i in range(len(request.POST[k])):
				c = request.POST[k][i]
				seller = request.POST.get('seller'+k_id,None)
				orderid = request.POST.get('orderid'+k_id,None)
				if all([c,seller,orderid]):
					sel = check_username(seller)
					if sel:
						ds = Dispute.objects.filter(buyer = u , seller = sel,solved = False,order = o)
						if len(ds) == 0:

							d = Dispute.objects.create(buyer = u , seller= sel,order = o )
						else:
							d = ds[0]
						print('########################## {0} $$$$$$$$$$$$$$$$$$$$$$$$'.format(sel.username))
						notif = Notifications.objects.create(user= sel,content = 'Opened Dispute for Order #'+str(o.id), url = '/user/dispute/'+str(d.id))
						notif.save()
						oh = o.orderholder_set.filter(seller = seller)
						if len(oh) == 0:
							#input('coninue here')
							continue
						else:
							try:
								od = oh[0].orderdetails_set.filter(id = int(orderid))
								if len(od) == 0:
									print('continue')
									continue
								else:
									od = od[0]
									d_o = DisputedOrders.objects.create(dispute = d , order_details=od)
									d.save()
									d_o.save()

							except Exception as e:
								print(e)
								continue
						

					else:
						continue
		return redirect('/user/dashboard')


class SendMessage(View):

	def post(self,request,orderid,disputeid):
		if not isLogged(request) and len(request.user.username ) == 0:
			print('here')
			return redirect('/products')

		msg = request.POST.get('message',None)
		if len(request.user.username ) == 0:
			u = get_user(request.session['email'])
			
			o = u.orderbuyer.filter(id=orderid )
			if len(o) == 0:
				admin = False
				username = u.username
				o = u.orderseller.filter(id=orderid )
				if len(o) != 0:
					o = o[0]
				else:	
					return redirect('/products')
			else:
				admin = False
				username = u.username
				o = o[0]
		else:
			o = Order.objects.filter(id = orderid)
			if len(o) == 0:
				return redirect('/adminpanel/dashboard')
			else:
				admin = True
				username = ''
				o = o[0]


		d = o.dispute_set.filter(id=disputeid)
		if len(d) == 0:
			if admin:
				return redirect('/adminpanel/dashboard')
			return redirect('/products')
		else:
			d = d[0]

		
		if msg and len(msg.replace(' ','')) > 0 and not d.solved:
			if admin or d.order.buyer.username == username or d.order.seller.username == username :
				if admin:
					m = Messages.objects.create(dispute= d, sender = None ,message = msg)
				else:
					m = Messages.objects.create(dispute= d, sender = u, message = msg)
				

				if d.order.buyer.username == username:
					r = Receivers.objects.create(msg = m , user = d.order.seller)
					m.save()
					r.save()
				elif d.order.seller.username == username:
					r = Receivers.objects.create(msg = m , user = d.order.buyer)
					m.save()
					r.save()
				else:
					m.fromadmin = True
					m.save()
					r = Receivers.objects.create(msg = m , user = d.order.seller)
					r.save()
					r = Receivers.objects.create(msg = m , user = d.order.buyer)
					r.save()
				
			


		if admin:
			return redirect('/adminpanel/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
		else:
		
			return redirect('/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))

		
class MarkSolved(View):

	def post(self,request,orderid,disputeid):
		if not isLogged(request) and len(request.user.username ) == 0:
			print('here')
			return redirect('/products')
		
		
		if len(request.user.username ) == 0:
			u = get_user(request.session['email'])
			
			o = u.orderbuyer.filter(id=orderid )
			if len(o) == 0:
				admin = False
				username = u.username
				
				return redirect('/products')
			else:
				admin = False
				username = u.username
				o = o[0]
		else:
			o = Order.objects.filter(id = orderid)
			if len(o) == 0:
				return redirect('/adminpanel/dashboard')
			else:
				admin = True
				username = ''
				o = o[0]


		d = o.dispute_set.filter(id=disputeid)
		if len(d) == 0:
			if admin:
				return redirect('/adminpanel/dashboard')
			return redirect('/products')
		else:
			d = d[0]
		d.solved = True
		d.save()
		o = d.order
		ds = o.dispute_set.filter(solved = False)
		if len(ds) == 0:
			o.released = True
			
			
			details = o.orderdetails_set.all()
			total = 0
			for detail in details:
				total += ((detail.price * detail.quantity) - detail.fee)
			seller = o.seller
			
			seller.balance += (total / get_price())
			seller.activebalance += (total / get_price())
			seller.balance = round(seller.balance ,8)
			seller.activebalance = round(seller.activebalance,8)
			seller.save()
			
			o.buyer_reviewed = True
			rate = int(o.total*0.1)
			b = o.buyer
			total , lvl , remaining , lvlxp , new_lvl = handle_xp(b.buyer_xp , b.buyer_level,rate)
			print('here #####@@@@@@@@')
			print(total)
			print(lvl)
			print(remaining)
			print(lvlxp)
			print(new_lvl)
			print(rate)
			print('@@@@@@@@@@@')
			b.buyer_xp = total
			if new_lvl > b.buyer_level:
				b.buyer_upgradable = True
			""" if int(rating) < 3:
				b.buyer_negative += 1
			else:
				b.buyer_positive += 1 """
			b.save()
			
			o.completed = True
			o.status = 'completed'
			notif = Notifications.objects.create(user= o.buyer,content = 'Dispute Solved for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
			notif.save()
			notif = Notifications.objects.create(user= o.seller,content = 'Dispute Solved for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
			notif.save()
			notif = Notifications.objects.create(user= seller,content = 'Completed Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			notif = Notifications.objects.create(user= o.buyer,content = 'Completed Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			o.save()
		return redirect('/user/orderdetail/'+str(o.id))

class Invite(View):

	def post(self,request,orderid,disputeid):
		if not isLogged(request) and len(request.user.username ) == 0:
			print('here')
			return redirect('/products')
		
		
		if len(request.user.username ) == 0:
			admin = False
			u = get_user(request.session['email'])
			
			o = u.orderbuyer.filter(id=orderid )
			if len(o) == 0:
				o = u.orderseller.filter(id = orderid)
				if len(o) == 0:
					username = u.username
					
					return redirect('/products')
				else:
					admin = False
					username = u.username
					o = o[0]
			else:
				admin = False
				username = u.username
				o = o[0]
		else:
			o = Order.objects.filter(id = orderid)
			if len(o) == 0:
				return redirect('/adminpanel/dashboard')
			else:
				admin = True
				username = ''
				o = o[0]


		d = o.dispute_set.filter(id=disputeid)
		if len(d) == 0:
			if admin:
				return redirect('/adminpanel/dashboard')
			return redirect('/products')
		else:
			d = d[0]
		d.invited = True
		d.save()
		notif = Notifications.objects.create(user= o.buyer,content = 'Invited Admin to dispute for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
		notif.save()
		notif = Notifications.objects.create(user= o.seller,content = 'Invited Admin to dispute for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
		notif.save()
		
		
		return redirect('/user/dispute/'+str(d.order.id) + '/' + str(d.id))

class Refund(View):
	def post(self,request,orderid,disputeid):
		if not isLogged(request) and len(request.user.username ) == 0:
			print('here')
			return redirect('/products')
		
		
		if len(request.user.username ) == 0:
			u = get_user(request.session['email'])
			admin = False
			username = u.username
			o = u.orderseller.filter(id=orderid )
			if len(o) != 0:
				o = o[0]
			else:	
				return redirect('/products')
		else:
			o = Order.objects.filter(id = orderid)
			if len(o) == 0:
				return redirect('/adminpanel/dashboard')
			else:
				admin = True
				username = ''
				o = o[0]


		d = o.dispute_set.filter(id=disputeid)
		if len(d) == 0:
			if admin:
				return redirect('/adminpanel/dashboard')
			return redirect('/products')
		else:
			d = d[0]
			if d.solved:
				return redirect('/user/dispute/'+str(d.order.id) + '/' + str(d.id))
		d.solved = True
		d.save()
		o = d.order
		ds = o.dispute_set.filter(solved = False)
		if len(ds) == 0:
			o.released = True
			
			
			details = o.orderdetails_set.all()
			total = 0
			for detail in details:
				total += ((detail.price * detail.quantity) - detail.fee)
			buyer = o.buyer
			
			buyer.balance += (total / get_price())
			buyer.activebalance += (total / get_price())
			buyer.balance = round(buyer.balance ,8)
			buyer.activebalance = round(buyer.activebalance,8)
			buyer.save()
			o.completed = True
			o.status = 'refunded'
			notif = Notifications.objects.create(user= o.buyer,content = 'Dispute Solved for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
			notif.save()
			notif = Notifications.objects.create(user= o.seller,content = 'Dispute Solved for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
			notif.save()
			notif = Notifications.objects.create(user= o.seller,content = 'Refunded Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			notif = Notifications.objects.create(user= o.buyer,content = 'Refunded Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
			notif.save()
			o.save()
		return redirect('/user/orderdetail/'+str(o.id))


class Review(View):

	def post(self,request,orderid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		o = u.orderbuyer.filter(id=orderid )
		if len(o) == 0:
			o = u.orderseller.filter(id=orderid )
			if len(o) != 0:
				o = o[0]
			else:	
				return redirect('/products')
		else:
			o = o[0]

		scale = {
			'1':10,
			'2':25,
			'3':40,
			'4':70,
			'5':100
		}
		buyer = request.POST.get('buyer',None)
		seller = request.POST.get('seller',None)
		rating = request.POST.get('rating',None)
		feedback = request.POST.get('feedback',None)
		print(request.POST)
		if all([buyer,rating,feedback]):
			if o.buyer.username == buyer and o.seller.username == u.username and False:
				try:
					if o.buyer_reviewed:
						return redirect('/user/orderdetail/'+str(orderid))
					
					o.buyer_score = int(rating)
					o.buyer_feedback = feedback
					o.buyer_reviewed = True
					rate = int(o.total*scale[rating]*0.01)
					b = o.buyer
					total , lvl , remaining , lvlxp , new_lvl = handle_xp(b.buyer_xp , b.buyer_level,rate)
					print('here #####@@@@@@@@')
					print(total)
					print(lvl)
					print(remaining)
					print(lvlxp)
					print(new_lvl)
					print(rate)
					print('@@@@@@@@@@@')
					b.buyer_xp = total
					if new_lvl > b.buyer_level:
						b.buyer_upgradable = True
					if int(rating) < 3:
						b.buyer_negative += 1
					else:
						b.buyer_positive += 1
					b.save()
					o.save()
					return redirect('/user/orderdetail/'+str(orderid))
					 
				except Exception as e:
					print(str(e))
					return redirect('/user/orderdetail/'+ str(orderid))
			else:
				return redirect('/user/orderdetail/'+ str(orderid))
		elif all([seller,rating,feedback]):
			if o.buyer.username == u.username and o.seller.username ==  seller :
				try:
					sel = o.seller
					
					if sel:
						
						if o.seller_reviewed:
							return redirect('/user/orderdetail/'+str(orderid))

						o.seller_score = int(rating)
						o.seller_feedback = feedback
						o.seller_reviewed = True
						rate = int(o.total*scale[rating]*0.01)
						
						total , lvl , remaining , lvlxp , new_lvl = handle_xp(sel.seller_xp , sel.seller_level,rate,buyer=False)
						
						sel.seller_xp = total
						if new_lvl > sel.seller_level:
							sel.seller_upgradable = True
						if int(rating) < 3:
							sel.seller_negative += 1
						else:
							sel.seller_positive += 1
						
						sel.save()
						o.save()
						return redirect('/user/orderdetail/'+str(orderid))
					else:
						print('here')
						return redirect('/user/orderdetail/'+ str(orderid))
					 
				except Exception as e:
					print(str(e))
					return redirect('/user/orderdetail/'+ str(orderid))
			else:
				#input('first check')
				return redirect('/user/orderdetail/'+ str(orderid))
		else:
			print(request.POST)
			od = OrderDetail()
			
			return od.get(request,orderid,feedbackerror=True)




###






####


# Seller Section


class PublicProfile(View):


	def count_comp_act(self,u):
		ohs = OrderHolder.objects.filter(seller = u.username )
		com = 0
		act = 0
		avg_score = 0
		earned = 0
		for oh in ohs:
			if oh.order.completed:
				com += 1
			elif oh.order.status == 'active':
				act += 1
			ods = oh.orderdetails_set.all()
			for od in ods:
				earned += round((od.price * od.quantity) - od.fee , 2)

			avg_score += oh.score
		if ohs.count() == 0:
			avg_score = 0
		else:
			avg_score = round(avg_score / ohs.count() ,2)
		

		score_perc = int( (avg_score  / 5 ) * 100 )		
		return com , act , avg_score , score_perc ,earned

	def get(self,request,sellerid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Seller Profile')
		u = CustomUser.objects.filter(id = sellerid , isseller = True)
		if len(u) == 0:
			return redirect('/products')
		else:
			u = u[0]


		c_os = u.orderseller.filter(completed = True)

		data['com'] = c_os.count()

		data['lvl'] = u.seller_level
		data['featured'] = formatprod(u.productcat_set.filter(featured = True))
		data['prods'] = formatprod(u.productcat_set.filter(featured= False))

		feedback = []
		avg = 0

		count = 0

		for o in c_os:
			if o.seller_reviewed:
				feedback.append(o)
				avg += o.seller_score
				count += 1
		if count == 0:
			avg = 0
		else:
			avg = avg / count



		data['avg'] = avg	

		data['feedback'] = feedback
		data['u'] = u




		return render(request,'seller/publicprofile.html',data)


class AuctionPage(View):
	

	def get(self,request,auctid):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Seller Profile')
		auction = Auction.objects.filter(closed = False, id = auctid)
		u = get_user(request.session.get('email',''))
		if len(auction) == 0:
			return redirect('/products')
		else:
			auction = auction[0]


		hour , minute , second = get_date(auction.date)
		s = Settings.objects.all()[0]
		if (hour * 3600 + minute * 60 + second ) > s.auction_duration * 60:
			hour , minute ,second = 0,0,0
		else:
			hour, minute , second = format_sec(s.auction_duration * 60 - (hour * 3600 + minute * 60 + second ) )

		if auction.closed:
			return redirect('/products')

		mybids = bids.objects.filter(auction= auction , user=u).order_by('-bid_price')
		topbids = bids.objects.filter(auction = auction ).order_by('-bid_price')

		data['bids'] = mybids
		data['topbids'] = topbids
		data['timeleft'] = '{0}:{1}:{2}'.format(hour,minute,second)
		
		data['auction'] = auction
		return render(request,'auction.html',data)


	def post(self,request,auctid):
		if not isLogged(request):
			print('here')
			return redirect('/products')

		auction = Auction.objects.filter(closed = False, id = auctid)
		u = get_user(request.session.get('email',''))
		if len(auction) == 0:
			print('no auction ?')
			return redirect('/products')
		else:
			auction = auction[0]

		bidid = request.POST.get('bidid',None)
		amount = request.POST.get('amount',None)
		print(amount)

		if all([bidid,amount]):
			try:
				bidid = int(bidid)
				amount = float(amount)
				if amount == 0:
					return redirect('/auction/'+str(auction.id))
				b = bids.objects.filter(user = u , auction = auction, id = bidid)
				if len(b)==0:
					print('here @@@@@')
					return redirect('/auction/'+str(auction.id))
				else:
					b = b[0]
					print('checks')
					print(b.bid_price)
					if amount > b.bid_price:

						diff = amount - b.bid_price
						print(diff)
						price = round( diff / get_price() , 8)
						print(price)
						if price <= u.activebalance:
							u.balance -= price
							u.activebalance -= price
							u.balance = round(u.balance ,8)
							u.activebalance = round(u.activebalance,8)
							b.bid_price = amount
							b.save()
							u.save()
						else:
							return redirect('/auction/'+str(auction.id))


					elif amount < b.bid_price:
						return redirect('/auction/'+str(auction.id))
						diff = b.bid_price - amount
						price = round( diff / get_price() , 8)	
						u.balance += price
						u.activebalance += price
						u.balance = round(u.balance ,8)
						u.activebalance = round(u.activebalance,8)
						b.bid_price = amount
						b.save()
						u.save()
					
					return redirect('/auction/'+str(auction.id))

			except:
				return redirect('/auction/'+str(auction.id))
		else:
			return redirect('/auction/'+str(auction.id))




class AuctionBid(View):

	def get(self,request,auctid ,error=False,msg=''):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		data = req_resp(request,'Bid Page')
		auction = Auction.objects.filter(closed = False, id = auctid)
		if len(auction) == 0:
			return redirect('/products')
		else:
			auction = auction[0]

		u = get_user(request.session['email'])

		prods = u.productcat_set.filter(show = True , admin_list = True)
		prodss = []
		for prod in prods:
			if (auction.bids_set.filter(product = prod).count() == 0):
				prodss.append(prod)
		
		if len(prodss) == 0:
			return redirect('/auction/'+ str(auction.id))

		data['auction'] = auction
		data['prods'] = prodss
		data['err'] = error
		data['errmsg'] = msg

		return render(request,'auctionproduct.html',data)


	def post(self,request,auctid):
		if not isLogged(request):
			print('here')
			return redirect('/products')

		u = get_user(request.session['email'])

		#auctid = request.POST.get('auctid',None)
		prodid = request.POST.get('prodid',None)
		amount = request.POST.get('bid',None)

		if all([auctid, prodid, amount]):
			try:
				#auctid = int(auctid)
				prodid = int(prodid)
				amount = float(amount)
				if amount == 0:
					return self.get(request,auctid,error = True , msg='You can\'t bid with 0 ' )
			except:
				return redirect('/products')
			
			auction = Auction.objects.filter(id = auctid)
			prod = u.productcat_set.filter(id = prodid)
			if len(auction) == 0 :
				return redirect('/products')
			elif len(prod) == 0:
				return self.get(request,auctid,error = True , msg='Product selected not found' )
			else:
				auction = auction[0]
				prod = prod[0]
			price = round( amount / get_price() , 8)
			if price <= u.activebalance:
				b = bids.objects.create(auction = auction,user = u , product = prod,bid_price = amount)
				u.balance -= price
				u.activebalance -= price
				u.balance = round(u.balance ,8)
				u.activebalance = round(u.activebalance,8)
				u.save()
				b.save()
				data = req_resp(request,'Bid confirmation')
				return render(request,'confirmation_bid.html',data)
			else:
				return self.get(request,auctid,error = True , msg='Not enough balance' )

				


		else:
			return redirect('/products')













class SellerDashboard(View):

	def count_disp(self,u):
		os = u.order_set.all()
		count = 0
		for o in os:
			if len(o.dispute_set.all()) != 0:
				count += 1
		
		return count

	def count_comp_act(self,u):
		ohs = OrderHolder.objects.filter(seller = u.username )
		com = 0
		act = 0
		avg_score = 0
		earned = 0
		for oh in ohs:
			if oh.order.completed:
				com += 1
			elif oh.order.status == 'active':
				act += 1
			ods = oh.orderdetails_set.all()
			for od in ods:
				earned += round((od.price * od.quantity) - od.fee , 2)

			avg_score += oh.score
		if ohs.count() == 0:
			avg_score = 0
		else:
			avg_score = round(avg_score / ohs.count() ,2)
		

		score_perc = int( (avg_score  / 5 ) * 100 )		
		return com , act , avg_score , score_perc ,earned


	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		prods = u.productcat_set.filter(show= False)
		for prod in prods:
			prod.delete()
		data = req_resp(request,'Seller Dashboard')
		data2 = formatprod(getProductUser(request.session['email']))
		data['products'] = data2
		
		if not u.isseller:
			return redirect('/user/dashboard')

		
		trs_deposit  = u.transactions_set.filter(tr_type='deposit').order_by('-date')
		trs_withdraw = u.transactions_set.filter(tr_type='withdraw').order_by('-date')
		data['deposit_trs'] = trs_deposit
		data['withdraw_trs'] = trs_withdraw
		completed = []
		active = []
		ds = []
		avg_score = 0
		count = 0
		earned = 0
		com = 0
		act = 0
		os = Order.objects.filter(seller = u )
		for o in os:
			if o.completed:
				completed.append(o)
				com += 1
				obj = o.orderdetails_set.all()[0]
				earned += round((obj.price * obj.quantity ) - obj.fee , 2)
			elif o.status == 'active':
				active.append(o)
				act += 1
			if o.dispute_set.filter(solved = False).count() > 0:
				ds.append(o.dispute_set.filter(solved = False)[0])
			if o.seller_reviewed:
				avg_score += o.seller_score
				count += 1
			
		if count == 0:
			pass
		else:
			avg_score = round(avg_score / count , 2)
		s_perc = int ((avg_score / 5 ) * 100)
		s = Settings.objects.all()[0]
		total , lvl , remaining , lvlxp ,max_lvl = handle_xp(u.seller_xp , u.seller_level)
		data['lvl'] = lvl
		data['rem'] = remaining
		data['lvlxp'] = lvlxp
		data['upgradable'] = u.seller_upgradable
		data['completed'] = completed
		data['active'] = active
		data['disputed'] = ds
		data['up_cost'] = s.upgrade_price_seller
		data['stickprice'] = s.stick_price
		
		#com , act,avg_score ,s_perc ,earned  = self.count_comp_act(u)
		data['avg_score'] = avg_score
		data['score_perc'] = s_perc
		data['earned'] = earned
		data['com_trades'] = com
		try:
			data['com_rate'] = round((com / os.count()) * 100 , 2)
			data['act_rate'] = round((act / os.count()) * 100,2)
			data['disp_rate'] = round((len(ds) /  os.count()) * 100 , 2)
		except ZeroDivisionError:
			data['com_rate'] = 0
			data['act_rate'] = 0
			data['disp_rate'] = 0
		return render(request,'seller/dashboardseller.html',data)


def upgrade_seller_lvl(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	s = Settings.objects.all()[0]
	total , lvl , remaining , lvlxp , new_lvl = handle_xp(u.seller_xp , u.seller_level)
	up_price = round(s.upgrade_price_seller / get_price() , 8) 
	if remaining == 0 and u.seller_upgradable and u.activebalance >= up_price :
		u.seller_level += 1
		u.seller_upgradable = False
		u.activebalance -= up_price
		u.balance -= up_price
		u.balance = round(u.balance ,8)
		u.activebalance = round(u.activebalance,8)
		u.save()
	elif u.activebalance < up_price:
		return balance_error(request,cost =s.upgrade_price_seller , error = True )

	return redirect('/seller/dashboard')




class MySellings(View):


	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		if not u.isseller:
			return redirect('/user/dashboard')
		data = req_resp(request,'My Sellings')
		data2 = formatprod(getProductUser(request.session['email']))
		data['products'] = data2


		
		return render(request,'seller/mysellings.html',data)


class NewSelling(View):

	
	
	def createProduct(self,email):
		u = get_user(email)
		lst1 = ascii_lowercase + ascii_letters + digits
		lst = [random.choice( lst1) for _ in range(128)]
		idd = ''.join(lst)
		cat = Category.objects.all()[0]
		p = ProductCat.objects.create(productid = idd,user= u, category = cat )
		p.save()
		return idd,p


	def prepare(self,p):
		data = {
			'pname' : p.name,
			'pay' : p.payment_type,
			'price' : str(p.price),
			'shown' : p.show,
			'catid' : p.category.id,
			'replace' : p.replace_time,
			'accs' : [],
			'cats' : []
		}

		accs = p.accounts_set.all()
		cats = Category.objects.all()

		for cat in cats:
			data['cats'].append(cat)

		
		if len(accs) == 0:
			pass
		else:
			#headers= p.category.category_headers.split(',')
			for acc in accs:
				splited = acc.line.split(',')
				""" if len(splited) > len(headers):
					for _ in range(len(splited) - len(headers)):
						headers.append('')
				else:
					for _ in range( len(headers) - len(splited)):
						splited.append('') """
				dt = []
				for i in range(len(splited)):
					dt.append(splited[i])
				
				data['accs'].append(dt)
		


		return data		


	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		if not u.isseller:
			return redirect('/user/dashboard')
		new = request.GET.get('new',True)
		print('@@@@@@@@@@@@@@')
		print(new)
		p = False
		if request.session.get('uuid',False):
			p = getProduct(request.session['uuid'],request.session['email'])
			if p and new != 'false':
				print('deleting ....')
				p.delete()
				p = False
		
		
		if not p:
			idd , p = self.createProduct(request.session['email'])
			request.session['uuid'] = idd		
		data = req_resp(request,'New Product')
		dt2 = self.prepare(p)
		data = {**data,**dt2}
		return render(request,'seller/newselling.html',data)

	def post(self,request):
		u = get_user(request.session['email'])
		if not u.isseller:
			return redirect('/user/dashboard')
		p = getProduct(request.session['uuid'],request.session['email'])
		name = request.POST.get('name',None)
		#tags = request.POST.get('tags',None)
		#details = request.POST.get('details',None)
		#extra = request.POST.get('extra',None)
		payment = request.POST.get('payment',None)
		price = request.POST.get('price',None)
		replace = request.POST.get('replace',None)
		try:
			replace = int(replace)
			price = float(price)
		except:
			replace = None
			price = None
		catid = request.POST.get('catid',None)
		if all([catid,name,payment,price,replace]):
			try:
				cat = Category.objects.filter(id = int(catid))
				if len(cat) != 0:
					cat = cat[0]
					p.category = cat
			except:
				pass
			if price <= 0:
				return cancel_prod(request,delete=True)
			p.name = name 
			p.payment_type = payment
			p.price = price
			p.replace_time = replace
			if len([False for x in [catid,name,payment,price] if x == '']) == 0:
				p.show = True
			p.save()
			return redirect('/seller/newselling?new=false')
		else:
			return redirect('/seller/newselling')


def cancel_prod(request,delete=False):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	if not u.isseller:
		return redirect('/user/dashboard')
	
	if request.session.get('uuid',False):
		p = getProduct(request.session['uuid'],request.session['email'])
		input(p.show)
		if not p.show or  delete :
			p.delete()
		return redirect('/seller/dashboard')
	else:
		return redirect('/seller/dashboard')




def feature_prod(request,prodid):
	if not isLogged(request):
			print('here')
			return redirect('/products')
	u = get_user(request.session['email'])
	if not u.isseller:
		return redirect('/user/dashboard')

	p = ProductCat.objects.filter(user = u , productid = prodid)
	if len(p) == 0:
		return redirect('/seller/dashboard')
	else:
		s = LevelManager.objects.filter(lvl = u.seller_level )
		if len(s) == 0:
			return redirect('/seller/dashboard')
		else:
			s = s[0]
		p = p[0]
		up_price = round(s.feature_price / get_price() , 8)
		if u.activebalance >= up_price and not p.sticked:
			u.activebalance -= up_price
			u.balance -= up_price
			u.balance = round(u.balance ,8)
			u.activebalance = round(u.activebalance,8)
			u.save()
			
			p.sticked = True
			p.save()
		return redirect('/seller/dashboard')
			


def addaccount(request):
	u = get_user(request.session['email'])
	if not u.isseller:
		return redirect('/user/dashboard')
	if request.method == 'POST':
		idd = request.session.get('uuid',False)
		if idd:
			p = getProduct(request.session['uuid'],request.session['email'])
			data_cont = request.POST.get('data',None)
			separator = request.POST.get('separator',None)
			if not separator:
				separator = ','
			#password = request.POST.get('password',None)
			if all([data_cont]):
				for data in data_cont.split('\n'):

					splited_d = data.split(separator)
					print(splited_d)
					
					acc = p.accounts_set.create(line = ','.join(splited_d))
					acc.save()
				return redirect('/seller/newselling?new=false')
			else:
				return HttpResponse('something is wrong')
		else:
			return HttpResponse('something is wrong')
	else:
		return HttpResponse('not allowed')
def confirmChange(request):
	u = get_user(request.session['email'])
	if not u.isseller:
		return redirect('/user/dashboard')
	if request.method == 'POST':

		del request.session['uuid']
		return redirect('/seller/dashboard')
	else:
		return HttpResponse('not allowed')



class SellerProductDetail(View):
	def get(self,request):
		if not isLogged(request):
			print('here')
			return redirect('/products')
		u = get_user(request.session['email'])
		if not u.isseller:
			return redirect('/user/dashboard')
		data = req_resp(request,'Seller Product Detail')
		return render(request,'seller/sellerproductdetails.html',data)

def restock(request,prodid):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	if not u.isseller:
		return redirect('/user/dashboard')
	request.session['uuid'] = prodid
	request.session['restock'] = True
	return redirect('/seller/newselling?new=false')


# product view 


	

class Products(View):

	



	
	def get(self,request,ps=None,sticky=None,catid = -1):
		if not isLogged(request):
			print('here')
			return redirect('/login')
		data = req_resp(request,'Products')
		
		if ps != None:
			data2 = formatprod(ps)
		else:
			data2 = formatprod(ProductCat.objects.filter(show=True,admin_list=True,featured = False))


		if sticky:
			sticky_d = formatprod(sticky)
		else:
			sticky_d = []
		featureds = ProductCat.objects.filter(show = True, admin_list = True , featured = True , featured_show = False)
		""" if featureds.count() == 4 :
			for x in ProductCat.objects.filter(show = True, admin_list = True , featured = True , featured_show = True):
				x.featured_show = False
				x.save()
			featureds = ProductCat.objects.filter(show = True, admin_list = True , featured = True , featured_show = False)
			 """
		if not ps:
			f_list = []
			for featured in featureds:
				if featured.accounts_set.all().count() == 0:
					continue
				else:
					f_list.append(featured)
					featured.featured_show = True
					featured.save()

					if len(f_list) == 4:
						break
			
			if len(f_list) < 4 :
				for x in ProductCat.objects.filter(show = True, admin_list = True , featured = True , featured_show = True):
					x.featured_show = False
					x.save()
				for featured in ProductCat.objects.filter(show = True, admin_list = True , featured = True , featured_show = False):
					if featured.accounts_set.all().count() == 0:
						continue
					else:
						f_list.append(featured)
						featured.featured_show = True
						featured.save()

						if len(f_list) == 4:
							break
				
			if len(f_list) == 0:
				data['featured'] = False
			else:
				data['featured'] = True
			data3 = formatprod(f_list)
			data['featureds'] = data3
		else:
			data['featured'] = False
		cats = Category.objects.all()

		data['products'] = data2
		data['stickys'] = sticky_d
		
		data['cats'] = cats
		print(catid)
		data['chosen'] = catid
		#print(data)
		
		return render(request, 'products.html',data)
		


def search(request):
	if not isLogged(request):
		print('here')
		return redirect('/login')
	q = request.GET.get('q',None)
	if q:
		ps = ProductCat.objects.filter(name__contains= q,show=True,admin_list = True)
		print(q)
		print(len(ps))
		p_v = Products()
		return p_v.get(request,ps=ps)
	else:
		return redirect('/products')

def filter_search(request):
	if not isLogged(request):
		print('here')
		return redirect('/login')
	catid = request.POST.get('cat_filter',None)
	print(catid)
	if catid:
		try:
			catid = int(catid)
			if catid >= 0:
				cat = Category.objects.filter(id = catid)
				if len(cat) == 0:
					return redirect('/products')
				else:
					print('here')
					cat = cat[0]
				ps1 = cat.category.filter(show=True,admin_list=True,sticked = True)
				ps2 = cat.category.filter(show=True,admin_list=True,sticked=False)
				ps =  list(ps2)
				p_v = Products()
				return p_v.get(request,ps=ps,sticky=list(ps1),catid=catid)

				
			else:
				return redirect('/products')
		except Exception as e:
			print(str(e))
			return redirect('/products')
	else:
		return redirect('/products')


#####


def markallread(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	url = request.META.get('HTTP_REFERER',False)
	notifs = Notifications.objects.filter(user = u)
	for notif in notifs:
		notif.read = True
		notif.save()
	if url:
		return redirect(url)
	else:
		return redirect('/products')


def markread(request,notifid):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	u = get_user(request.session['email'])
	notif = Notifications.objects.filter(id = notifid,user= u)
	#url = request.META.get('HTTP_REFERER',False)
	url = False
	print(url)
	if len(notif) == 0:
		pass
	else:
		notif = notif[0]
		notif.read = True
		url = notif.url
		notif.save()

	if url:
		return redirect(url)
	else:
		return redirect('/products')


def news(request):
	data = req_resp(request,'News')
	return render(request, 'news.html',data)









def sellertoplist(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	data = req_resp(request,'Seller Top List')
	sellers = CustomUser.objects.filter(isseller=True).order_by('-seller_positive')
	s = {
	}
	for sel in sellers :
		s[sel] = [get_avg_rating(sel),get_sel_orders(sel)]

	temp = dict(sorted(s.items(), key=lambda item: (item[1][0],item[1][1]),reverse=True))
	data['sellers'] = [x for x in temp.keys()]
	return render(request, 'sellertoplist.html',data)

def support(request,show=False,success=False,msg=''):
	if not isLogged(request):
		print('here')
		return redirect('/products')
	data = req_resp(request,'Support')
	data['show'] = show
	data['success'] = success
	data['msg'] = msg
	return render(request, 'support.html',data)









def transactionhistory(request):
	if not isLogged(request):
		print('here')
		return redirect('/products')

	u = get_user(request.session['email'])
	trs = u.transactions_set.all()
	data = req_resp(request,'Transaction History')
	data['trs'] = trs
	return render(request, 'transactionhistory.html',data)






