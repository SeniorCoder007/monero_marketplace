# Generated by Django 3.1.5 on 2021-02-05 23:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0026_messages'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='disputedorders',
            name='dispute',
        ),
        migrations.RemoveField(
            model_name='disputedorders',
            name='order_details',
        ),
        migrations.RemoveField(
            model_name='messages',
            name='dispute',
        ),
        migrations.RemoveField(
            model_name='messages',
            name='sender',
        ),
        migrations.DeleteModel(
            name='Dispute',
        ),
        migrations.DeleteModel(
            name='DisputedOrders',
        ),
        migrations.DeleteModel(
            name='Messages',
        ),
    ]
