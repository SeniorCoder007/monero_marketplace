# Generated by Django 3.1.5 on 2021-02-02 22:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0018_auto_20210202_2201'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accounts',
            name='email',
        ),
        migrations.RemoveField(
            model_name='accounts',
            name='password',
        ),
        migrations.RemoveField(
            model_name='productcat',
            name='details',
        ),
        migrations.RemoveField(
            model_name='productcat',
            name='extra',
        ),
        migrations.RemoveField(
            model_name='productcat',
            name='tags',
        ),
        migrations.AddField(
            model_name='accounts',
            name='line',
            field=models.CharField(default='', max_length=255),
        ),
    ]
