# Generated by Django 3.1.5 on 2021-02-07 00:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0031_auto_20210207_0054'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='buyer_level',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='customuser',
            name='buyer_xp',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='customuser',
            name='seller_level',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='customuser',
            name='seller_xp',
            field=models.IntegerField(default=0),
        ),
    ]
