# Generated by Django 3.1.5 on 2021-02-24 21:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0045_productcat_featured_show'),
    ]

    operations = [
        migrations.CreateModel(
            name='Auction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('closed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='bids',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('winner', models.BooleanField(default=False)),
                ('bid_price', models.FloatField(default=0)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('auction', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='store.auction')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='store.productcat')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='store.customuser')),
            ],
        ),
    ]
