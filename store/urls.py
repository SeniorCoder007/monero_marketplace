from django.urls import path
from .views import *

urlpatterns = [
	path('',index),
	# Auth routes
    path('login', Login.as_view()),
	path('register', Register.as_view()),
	path('logout',logout),
	###

	# Seller Router
	path('seller/dashboard',SellerDashboard.as_view()),
	path('seller/mysellings',MySellings.as_view()),
	path('seller/newselling',NewSelling.as_view()),
	path('seller/productdetail',SellerProductDetail.as_view()),
	path('seller/addaccount',addaccount),
	path('seller/confirm',confirmChange),
	path('seller/restock/<str:prodid>',restock),
	path('seller/upgradeseller',upgrade_seller_lvl),
	path('seller/stick/<str:prodid>', feature_prod),
	path('seller/cancel',cancel_prod),
	
	# Buyer Router 
	path('user/dashboard',Dashboard.as_view()),
	path('user/dispute/<int:orderid>/<int:disputeid>',DisputeV.as_view()),
	path('user/myorders',myorders),
	path('user/orderdetail/<int:orderid>',OrderDetail.as_view()),
	path('user/settings',UserProfile.as_view()),
	#path('user/cart',Cartview.as_view()),
	path('user/checkoutlist',checkoutlist),
	path('user/paymentdetails',Paymentdetails.as_view()),
	path('user/history',transactionhistory),
	#path('user/addtocart/<str:prodid>',addtocart),
	#path('user/cart/delete/<int:idd>',deletecartelem),
	path('user/deposit',Deposit.as_view()),
	path('user/withdraw',Withdraw.as_view()),
	path('user/balance',Balance.as_view()),
	path('user/startdispute/<int:orderid>',StartDispute.as_view()),
	path('user/sendmessage/<int:orderid>/<int:disputeid>',SendMessage.as_view()),
	path('user/marksolved/<int:orderid>/<int:disputeid>',MarkSolved.as_view()),
	path('user/review/<int:orderid>',Review.as_view()),
	path('user/upgradebuyer',upgrade_buyer_lvl),
	path('user/upgrade',upgrade_to_seller),
	path('user/submitticket',SubmitTicket.as_view()),
	path('user/buy/<str:prodid>',BuyProduct.as_view()),
	path('user/search',search),
	path('user/publicprofile/<int:sellerid>',PublicProfile.as_view()),
	path('user/mytickets',get_user_tickets),
	path('user/refund/<int:orderid>/<int:disputeid>',Refund.as_view()),
	path('user/invite/<int:orderid>/<int:disputeid>',Invite.as_view()),
	path('user/markread/<int:notifid>',markread),
	path('user/markallread',markallread),
	
	

	
	###
	path('auction/<int:auctid>',AuctionPage.as_view()),
	path('startbid/<int:auctid>',AuctionBid.as_view()),
	path('news',news),
	path('products',Products.as_view()),
	path('products/filter',filter_search),
	path('sellertoplist',sellertoplist),
	path('support',support),
	
]

