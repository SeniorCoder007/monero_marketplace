from django.db import models
from django.contrib.auth.models import User
from .tools import encrypt_str

# Create your models here.
class CustomUser(User):
	pin = models.CharField(max_length=255)
	banned = models.BooleanField(default=False)
	btcdeposit = models.CharField(max_length=500,default='')
	btcwithdraw = models.CharField(max_length=500,default='')
	address = models.CharField(max_length=500,default='')
	xmrwithdraw = models.CharField(max_length=500,default='')
	totaldeposited = models.FloatField(default=0)
	balance = models.FloatField(default=0)
	activebalance = models.FloatField(default=0)
	withdrawn = models.FloatField(default=0)
	placed_for_withdraw = models.FloatField(default=0)
	buyer_positive = models.IntegerField(default=0)
	buyer_negative = models.IntegerField(default=0)
	isseller = models.BooleanField(default = False)
	buyer_level = models.IntegerField(default=1)
	buyer_xp = models.IntegerField(default=0)
	buyer_upgradable = models.BooleanField(default=False)
	seller_positive = models.IntegerField(default=0)
	seller_negative = models.IntegerField(default=0)
	seller_level = models.IntegerField(default=1)
	seller_xp = models.IntegerField(default=0)
	seller_upgradable = models.BooleanField(default = False)


	def set_pin(self,pin):
		self.pin = encrypt_str(pin)
	
	def confirm_pin(self,pin):
		if self.pin == encrypt_str(pin):
			return True
		else:
			return False

	def __str__(self):
		return self.email

class Category(models.Model):
	category_name = models.CharField(max_length=255)
	#category_headers = models.CharField(max_length=255,default='email,password')


	def __str__(self):
		return self.category_name 

class ProductCat(models.Model):
	user = models.ForeignKey(CustomUser,on_delete = models.CASCADE)
	category = models.ForeignKey(Category, on_delete= models.CASCADE , related_name= 'category')
	productid = models.CharField(max_length=255)
	name = models.CharField(max_length=255,default='')
	#tags = models.CharField(max_length=255,default='')
	#details = models.TextField(default='')
	#extra = models.TextField(default='')
	payment_type = models.CharField(max_length = 255,default='')
	price = models.FloatField(default=0)
	replace_time = models.IntegerField(default=0)
	show = models.BooleanField(default=False)
	admin_list = models.BooleanField(default=True)
	featured = models.BooleanField(default = False)
	sticked = models.BooleanField(default=False)
	featured_show = models.BooleanField(default = False)

	def save(self, *args, **kwargs):
		print(list(args))
		print(dict(kwargs))
		super(ProductCat, self).save(*args, **kwargs)
	def __str__(self):
		return self.user.username


class Notifications(models.Model):
	user = models.ForeignKey(CustomUser, on_delete = models.CASCADE)
	content = models.CharField(max_length=255)
	url = models.CharField(max_length=255, default='#')
	read = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.content


class Transactions(models.Model):
	user = models.ForeignKey(CustomUser,on_delete= models.CASCADE)
	txid = models.CharField(max_length=300,default='')
	fee = models.FloatField(default=0)
	amount = models.FloatField(default=0)
	tr_type = models.CharField(max_length=100,default='')
	date = models.DateTimeField(auto_now_add=True)
	confirmed = models.BooleanField(default=False)
	sent = models.BooleanField(default=False)
	address = models.CharField(max_length=500,default='')


	def __str__(self):
		return '['+str(self.date)+']' + ' ' + self.txid 



class Cart(models.Model):
	user = models.ForeignKey(CustomUser,on_delete = models.CASCADE)
	productid = models.ForeignKey(ProductCat, on_delete=models.CASCADE)
	quantity = models.IntegerField(default = 1)

	def __str__(self):
		return self.productid.productid


class Order(models.Model):
	buyer = models.ForeignKey(CustomUser,on_delete = models.CASCADE,related_name='orderbuyer')
	total = models.FloatField(default=0)
	buyer_fee = models.FloatField(default = 0)
	buyer_reviewed = models.BooleanField(default=False)
	buyer_score = models.IntegerField(default= 0)
	buyer_feedback = models.TextField()
	seller = models.ForeignKey(CustomUser, on_delete= models.CASCADE,related_name = 'orderseller')
	seller_reviewed = models.BooleanField(default= False)
	seller_score= models.IntegerField(default=0)
	seller_feedback = models.TextField()
	status = models.CharField(max_length=255,default='active')
	released = models.BooleanField(default=False)
	completed = models.BooleanField(default=False)
	date = models.DateTimeField(auto_now_add=True)



class OrderDetails(models.Model):
	order = models.ForeignKey(Order, on_delete= models.CASCADE)
	prodname = models.CharField(max_length=255,default='')
	price = models.FloatField(default=0)
	quantity = models.IntegerField(default=0)
	replace_time = models.IntegerField(default=0)
	fee = models.FloatField(default = 0)


class Delivery(models.Model):
	orderdetails = models.ForeignKey(OrderDetails,on_delete= models.CASCADE)
	content = models.TextField(default='')

class Dispute(models.Model):
	order = models.ForeignKey(Order,on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)
	solved = models.BooleanField(default=False)
	invited = models.BooleanField(default=False)


class Messages(models.Model):
	dispute = models.ForeignKey(Dispute, on_delete=models.CASCADE)
	sender = models.ForeignKey(CustomUser,on_delete = models.CASCADE,related_name='sender',blank = True, null = True)
	message = models.TextField()
	fromadmin = models.BooleanField(default = False)
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		if self.sender:
			return '[{0}] {1}'.format(self.date,self.sender.username)
		else:
			return 'admin message'



class Receivers(models.Model):
	msg = models.ForeignKey(Messages,on_delete = models.CASCADE)
	user = models.ForeignKey(CustomUser, on_delete = models.CASCADE)
	date = models.DateTimeField(auto_now_add=True)


	def __str__(self):
		return self.user.username


class Auction(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	closed = models.BooleanField(default = False)

	def __str__(self):
		return str(self.date)

class bids(models.Model):
	auction = models.ForeignKey(Auction , on_delete=models.CASCADE)
	user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
	product = models.ForeignKey(ProductCat, on_delete = models.CASCADE)
	winner = models.BooleanField(default=False)
	bid_price = models.FloatField(default = 0)
	date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.user.username

""" class BuyerReview(models.Model):
	order = models.ForeignKey(Order, on_delete = models.CASCADE)
	seller = models.ForeignKey(CustomUser, on_delete = models.CASCADE)
	score = models.IntegerField(default= 0)
	feedback = models.TextField()

	




class OrderHolder(models.Model):
	order = models.ForeignKey(Order, on_delete = models.CASCADE)
	seller = models.CharField(max_length=255)
	reviewed = models.BooleanField(default=False)
	comm_review = models.IntegerField(default=0)
	service_review = models.IntegerField(default=0)
	recommend_review = models.IntegerField(default=0)
	reviewed = models.BooleanField(default = False)
	score = models.IntegerField(default= 0)
	feedback = models.TextField(default='')


class OrderDetails(models.Model):
	order_holder = models.ForeignKey(OrderHolder,on_delete= models.CASCADE)
	prodname = models.CharField(max_length=255,default='')
	price = models.FloatField(default=0)
	quantity = models.IntegerField(default=0)
	fee = models.FloatField(default = 0)
	

class Delivery(models.Model):
	orderdetails = models.ForeignKey(OrderDetails,on_delete= models.CASCADE)
	content = models.TextField(default='') """




""" 
	
class DisputedOrders(models.Model):
	dispute = models.ForeignKey(Dispute, on_delete=models.CASCADE,related_name='dorders')
	order_details = models.ForeignKey(OrderDetails,on_delete= models.CASCADE)





 """





	


class Accounts(models.Model):
	product = models.ForeignKey(ProductCat,on_delete=models.CASCADE)
	line = models.CharField(max_length=255,default='')