from celery import shared_task
from store.models import *
from .worker import *
from marketplace.node_handler import get_handler
from .helpers import *
from adminpanel.models import *








""" @app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
	sender.add_periodic_task(10.0, test.s('1'))
	sender.add_periodic_task(5.0, test.s('2'))
 """


@shared_task
def count_users():
	us = CustomUser.objects.all()
	print('count is : '+ str(len(us)))


@shared_task
def check_deposit():
	print('starting checker')
	w = worker()
	us = CustomUser.objects.filter(banned=False)
	for u in us:
		w.main(u)


@shared_task
def check_auction():
	auctions = Auction.objects.all().order_by('-date')
	s = Settings.objects.all()[0]
	if len(auctions) == 0:
		print('starting a new auction')
		auction = Auction.objects.create()
		auction.save()
	else:
		auction = auctions[0]
		if not auction.closed :
			print('still open')
		else:
			hour , minute ,second = get_date(auction.date)
			if (hour * 3600 + minute * 60 + second ) > s.restart_auction_days * 3600 * 24:
				print('starting new auction')
				auction = Auction.objects.create()
				auction.save()
			else:
				print('still not time')
			



@shared_task
def check_winner():
	s = Settings.objects.all()[0]
	auctions = Auction.objects.filter(closed = False).order_by('-date')
	if len(auctions) == 0:
		print('no auctions are active')
	else:
		auction = auctions[0]
		hour , minute ,second = get_date(auction.date)
		if  (hour * 3600 + minute * 60 + second ) > s.auction_duration * 60 :
			print('auction finished now getting winners')
			print('cleaning ...')
			prods = ProductCat.objects.filter(featured = True)
			for prod in prods:
				prod.featured = False
				prod.save()
			print('getting winners')
			bids = auction.bids_set.all().order_by('-bid_price')
			if len(bids) == 0:
				print('no bids no winners')
				pass
			else:
				winner_bids = []
				
				for x in bids:
					if len(winner_bids) == 4:
						break
					else:
						winner_bids.append(x)

				winner_bids = bids[:4]
				for winner in winner_bids:
					winner.winner = True
					winner.save()
					p = winner.product
					p.featured = True
					p.save()
				nws = auction.bids_set.filter(winner = False)
				for nw in nws:
					u = nw.user
					u.balance += nw.bid_price
					u.activebalance += nw.bid_price
					u.save()
			auction.closed = True
			auction.save()
				
		
		else:
			print('still auction not finished')








""" @shared_task(bind=True)
def handle_withdraw(self,idd):
	handler = get_handler('BTC')
	tr = Transactions.objects.filter(id = idd)
	if len(tr) == 0:
		pass
	else:
		tr = tr[0]
		print('starting withdraw to {0}'.format(tr.address))
		res = handler.send('sendtoaddress',tr.address,tr.amount,message = True)
		if res[0]:
			tr.sent = True
			tr.txid = ','.join(res)
			u = tr.user
			u.withdrawn += tr.amount
			u.placed_for_withdraw -= tr.amount
			tr.save()
			u.save()
			print('finalized')
				
			
		else:
			self.retry() """

@shared_task(bind=True)
def handle_withdraw(self,idd):
    handler = get_handler('XMR')
    tr = Transactions.objects.filter(id = idd)
    if len(tr) == 0:
        pass
    else:
        tr = tr[0]
        print('starting withdraw to {0}'.format(tr.address))
        #res = handler.send('sendtoaddress',tr.address,tr.amount,message = True)
        res = handler.send('transfer',destinations=[{'amount':tr.amount,'address':tr.address}],account_index = mon_index,unlock_time=0)
        if res:
            tr.sent = True
            tr.txid = ','.join(res)
            u = tr.user
            u.withdrawn += tr.amount
            u.placed_for_withdraw -= tr.amount
            tr.save()
            u.save()
            print('finalized')
                
            
        else:
            self.retry()


@shared_task(bind = True)
def check_active_orders(self):
	print("#### starting checker 2 ###")
	s = Settings.objects.all()[0]
	os = Order.objects.filter(completed = False)
	for o in os:
		
		hour , minute , second = get_date(o.date)
		details = o.orderdetails_set.all()
		if len(details) == 0:
			continue
		if (hour * 3600 + minute * 60 + second ) > details[0].replace_time * 3600:
			if o.dispute_set.filter(solved = False).count() > 0:
				continue
			else:
				o.released = True
				
				total = 0
				for detail in details:
					total += ((detail.price * detail.quantity) - detail.fee)
				seller = o.seller
				seller.balance += (total / btc_rate)
				seller.activebalance += (total / btc_rate)
				seller.balance = round(seller.balance ,8)
				seller.activebalance = round(seller.activebalance,8)
				seller.save()
				o.completed = True
				o.status = 'completed'
				#notif = Notifications.objects.create(user= o.buyer,content = 'Dispute Solved for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
				#notif.save()
				#notif = Notifications.objects.create(user= o.seller,content = 'Dispute Solved for Order #'+str(o.id), url = '/user/dispute/{0}/{1}'.format(str(o.id),str(d.id)))
				#notif.save()
				notif = Notifications.objects.create(user= seller,content = 'Completed Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
				notif.save()
				notif = Notifications.objects.create(user= o.buyer,content = 'Completed Order #'+str(o.id), url = '/user/orderdetail/'+str(o.id))
				notif.save()
				o.save()
		else:
			continue


@shared_task
def check_review():
	print("#### starting checker 3 ###")
	s = Settings.objects.all()[0]
	os = Order.objects.filter(seller_reviewed = False)
	for o in os:
		
		hour , minute , second = get_date(o.date)
		details = o.orderdetails_set.all()
		if len(details) == 0:
			continue
		if (hour * 3600 + minute * 60 + second ) > details[0].replace_time * 3600:
			if o.dispute_set.filter(solved = False).count() > 0:
				continue
			else:
				sel = o.seller
					
				if sel:
						
					if o.seller_reviewed:
						continue

					o.seller_score = 5
					o.seller_feedback = ''
					o.seller_reviewed = True
					rate = 100
						
					total , lvl , remaining , lvlxp , new_lvl = handle_xp(sel.seller_xp , sel.seller_level,rate,buyer=False)
						
					sel.seller_xp = total
					if new_lvl > sel.seller_level:
						sel.seller_upgradable = True
					
					sel.seller_positive += 1
						
					sel.save()
					o.save()
		else:
			continue


@shared_task
def price_updater():
	print('getting new price')
	set = Settings.objects.all()[0]
	t = Ticker()
	print('savinnng')
	set.btc_price = float(t.get_price())
	set.save()




@shared_task
def test2(arg):
	print(arg)