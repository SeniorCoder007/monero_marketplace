from rest_framework.serializers import ModelSerializer
from .models import CustomUser,ProductCat,Accounts,OrderDetails


class CustomUserSerializer(ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['username','email','pin','banned']

class ProductSerializer(ModelSerializer):
    class Meta:
        model = ProductCat
        fields = ['productid','name','payment_type','price','replace_time','featured','sticked']

class AccountSerializer(ModelSerializer):
    class Meta:
        model = Accounts
        fields = ['email','password']


class SellingSerializer(ModelSerializer):
    class Meta:
        model = OrderDetails
        fields = ['prodname','price','quantity','replace_time']