from marketplace.node_handler import *
from adminpanel.models import *
from marketplace.settings import *
import time
import datetime
import math




class worker():
    def __init__(self):
        self.handler = get_handler('BTC')
        self.x = get_handler('XMR')

    def get_transaction(self,address,trs):
        txids = []
        for tr in trs:
            if tr['address'] == address:
                print('getting transaction')
                print(tr['txid'])
                txids.append(tr['txid'])
                
                

            else:
                pass

        return txids

    def get_address_indice(self,address):
        resp = self.x.send('get_address_index',address=address)
        return resp['result']['index']['minor']



    def get_mon_received(self,address):
        indice  = self.get_address_indice(address)
        resp_dict = self.x.send('getbalance',account_index=mon_index,address_indices=[indice])
        res = resp_dict['result']
        addresses =  res['per_subaddress']
        for addr in addresses:
            if address == addr['address']:
                balance = addr['balance']
                return balance

    def get_mon_txid(self,address):
        resp  = self.x.send('get_transfers',account_index=mon_index,in_=True)
        in_list =  resp['result']['in']
        for l in in_list:
            if l['address']  == address:
                return [l['txid'],l['amount']]
    

    def get_mon_confirmation(self,txid):
        resp = self.x.send('get_transfer_by_txid',txid=txid,account_index=mon_index)
        return resp['result']['transfer']['confirmations']


    def custom_floor(self,am,prec):
        return math.floor(am* (10**prec))/ (10**prec)

    
    def main2(self,u):
        # checking deposit
        l = LevelManager.objects.filter(lvl = u.buyer_level)
        amount = self.handler.send('getreceivedbyaddress',u.btcdeposit,0)
        if amount > u.totaldeposited:
            print('new deposit')
            am = round(float(amount),8) - round( u.totaldeposited , 8)
            u.balance += (am * (1 - (l.deposit_fee / 100)))
            u.totaldeposited = round(float(amount),8)
            u.save()
        else:
            print('no deposit for {0}'.format(u.username))
        
        txids = self.get_transaction(u.btcdeposit,self.handler.send('listtransactions','*',999999999))
        if len(txids) > 0:
            for txid in txids:

                tr = self.handler.send('gettransaction',txid)
                print(tr)
                tr_cont = u.transactions_set.filter(txid = txid , tr_type = 'deposit' )
                if len(tr_cont) == 0:
                    tr_cont = u.transactions_set.create(txid= txid,amount = round(float(tr['amount']),8),tr_type='deposit',address = u.btcdeposit )
                else:
                    tr_cont = tr_cont[0]
                    if tr_cont.confirmed:
                        continue
                    else:
                        pass
                
                if int(tr['confirmations']) >= 3:
                    tr_cont.confirmed = True
                    u.activebalance += (tr_cont.amount * (1 - l.deposit_fee))
                    u.save()
                else:
                    tr_cont.confirmed = False
                tr_cont.save()
    def main1(self,u):
        # checking deposit
        #l = LevelManager.objects.filter(lvl = u.buyer_level)
        #amount = self.handler.send('getreceivedbyaddress',u.btcdeposit,0)
        amount = self.get_mon_received(address = u.address)
        if amount > u.totaldeposited:
            print('new deposit')
           # am = round(float(amount),8) - round( u.totaldeposited , 8)
            #u.balance += (am * (1 - (l.deposit_fee / 100)))
            am = (amount-u.totaldeposited) * (10**-12)
            
            u.balance += self.custom_floor(am , 12) 
            u.totaldeposited = amount
            u.save()
            txid = self.get_mon_txid(u.address)
            tr_cont = u.transactions_set.filter(txid = txid[0].strip() , tr_type = 'deposit' )
            if len(tr_cont) == 0:
                tr_cont = u.transactions_set.create(txid= txid[0].strip(),amount = self.custom_floor(am , 12) ,tr_type='deposit',address = u.address )
                tr_cont.save()
            else:
                pass

        else:
            print('no deposit for {0}'.format(u.username))
        
        tr_conts = u.transactions_set.filter(confirmed = False)
        if len(tr_conts) > 0:
            for tr_cont in tr_conts:
                print('txid ###################################################')
                print(tr_cont.txid.strip())
                confs = self.get_mon_confirmation(tr_cont.txid.strip())

                
                
                if confs >= 3:
                    tr_cont.confirmed = True
                    u.activebalance += tr_cont.amount 
                    u.save()
                else:
                    tr_cont.confirmed = False
                tr_cont.save()

    def main(self,u):
        self.main1(u)
        #self.main2(u)


        




    
    def send_transaction(self,add,amount,sett):
        print('sending')
        amount = float(amount)
        amount_out = round(float(amount),8)

        if amount_out !=  add.amount_sent:
            temp_amount = amount_out
            amount_out = amount_out - add.amount_sent
            amount_out = round(float(amount_out),8)

            print('sending amount : ',end=' ')
            print(amount_out)

            res = self.handler.send('sendtoaddress',add.address,amount_out,message = True)
            print(res)
            if res[0]:
                add.status = 'done'
                add.txid = ','.join(res)
                #if 'sent' in add.txid:
                #	add.txid = str(res[0])
                #else:
                #	add.txid += ','+str(res)
                add.amount_sent = temp_amount
                
                add.save()
            else:
                add.status = 'failed {0}'.format(res[-1])
                add.amount_sent = 0
                add.save()



    












    


    
