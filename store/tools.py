import hashlib
import string

def encrypt_str(s):
	return hashlib.sha256(s.encode()).hexdigest()

def verify_str(s):
    return any(list(map(lambda x: x in string.punctuation,s)))

