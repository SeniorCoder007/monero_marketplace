from .serializer import *
from .models import *
from marketplace.node_handler import *
from marketplace.settings import *
from marketplace.ticker import Ticker
from adminpanel.models import *
import datetime



def get_user(email):
	u = CustomUser.objects.filter(email=email)
	if len(u) == 0:
		return False
	else:
		return u[0]

def check_username(username):
	u = CustomUser.objects.filter(username=username)
	if len(u) == 0:
		return False
	else:
		return u[0]


def isLogged(request):
	print('checkin')
	
	if request.session.get('logged',False) and request.session.get('email',False):
		u = get_user(request.session['email'])
		if u:
			s = CustomUserSerializer(u)
			data = s.data
			print(data)
			return data
		else:
			return False
	else:
		return False

def generate_addr():
	h = get_handler('BTC')
	return h.send('getnewaddress')


def get_messages(u):
	rs = Receivers.objects.filter(user = u).order_by('-date')
	if len(rs)> 10:
		rs = rs[:10]
	
	return rs

def get_notifs(u):
	notifs = Notifications.objects.filter(user = u,read=False).order_by('-date')
	if len(notifs) > 30:
		notifs = notifs[:30]
	
	return notifs


def check_auction():
	auctions = Auction.objects.filter(closed = False).order_by('-date')
	if len(auctions) == 0:
		return False
	else:
		return auctions[0]
	




def req_resp(request,title):
	r = isLogged(request)
	btc_rate =  get_price()
	if r and not r['banned']:
		u = get_user(request.session['email'])

		count = len(u.cart_set.all())
		data = {
			'title':title,
			'username':r['username'],
			'email' : r['email'],
			'logged':request.session['logged'],
			'cart_count': str(count),
			'isseller': u.isseller,
			}
		notifs = get_notifs(u)
		msgsss = get_messages(u)
		s_not = []
		if len(notifs) >= 20:
			notifs = notifs[:20]
		else:
			notifs = notifs
		if len(notifs) >= 2:
			notifsss = notifs[:2]
		else:
			notifsss = notifs

		if len(msgsss) >= 10:
			msgss = msgsss[:2]
		else:
			msgss = msgsss
		
		data['btc_rate'] = btc_rate
		data['balancebtc'] = u.activebalance
		data['balanceusd'] = round(u.activebalance * btc_rate,2)
		data['totalbalancebtc'] = u.balance
		data['totalbalanceusd'] = round(u.balance * btc_rate,2) 
		data['msgss'] = msgss
		data['count_msgss'] = len(msgss)
		data['notifs'] = notifs
		data['count_not'] = len(notifs)
		data['notifss'] = notifsss
		data['auction'] = check_auction()
		return data
	else:
		try:
			request.session['logged'] = False
			del request.session['email']
		except:
			pass
		data = {
			'title' : title,
			'message' : [],
			'notifs': []
		}
		data['btc_rate'] = btc_rate
		return data



def getProduct(uuid,email):
	u = get_user(email)
	p = u.productcat_set.filter(productid = uuid)
	if len(p) == 0:
		return False
	else:
		return p[0]
		
def getProductUser(email):
	u = get_user(email)
	p = u.productcat_set.all()
	return p


def formatprod(ps):
	final = []
	for p in ps: 
		if not p.user.isseller:
			continue
		dt = ProductSerializer(p).data
		dt['price'] = dt['price']
		dt['quantity'] = len(p.accounts_set.all())
		dt['seller'] = p.user.username
		dt['seller_lvl'] = p.user.seller_level
		dt['positive'] = p.user.seller_positive
		dt['negative'] = p.user.seller_negative
		dt['idd'] = p.user.id
		
		final.append(dt)
	return final

def formatsingleprod(p):
	dt = ProductSerializer(p).data
	dt['price'] = dt['price']
	dt['username'] = p.user.username
	return dt


def get_product(prodid):
	p = ProductCat.objects.filter(productid = prodid)
	if len(p) == 0:
		return False
	else:
		return p[0]


def get_total(objs,buyer=True):

	total = 0
	for obj in objs:
		
		
		if buyer:
			total += obj.total + obj.buyer_fee
	return total

def format_cart(objs):
	final = []
	counter = 0
	total = 0
	for obj in objs:
		res = obj.productid
		if not res:
			obj.delete()
			continue

		dt = {}
		dt['id'] = obj.id
		dt['prodid'] = obj.productid.productid
		dt2 = formatsingleprod(res)
		dt['price'] = dt2['price']
		dt['name'] = dt2['name']
		""" dt['headers'] = res.category.category_headers """
		dt['quantity'] = obj.quantity
		dt['partialtot'] = dt2['price'] * obj.quantity
		dt['count'] = counter
		dt['seller'] = dt2['username']
		dt['replace_time'] = dt2['replace_time']
		counter += 1
		total += int(obj.quantity) * float(dt2['price'])
		final.append(dt)
	return final,total


def get_date( old):
	now = datetime.datetime.now(datetime.timezone.utc)
	res = now - old
	hour = res.days * 24 + (res.seconds // 3600)
	sec = res.seconds % 3600
	minute = sec // 60
	sec = sec % 60

	return hour,minute,sec

def format_sec(sec):
	hour = sec // 3600
	sec %= 3600
	minute = sec // 60
	sec %= 60
	return hour , minute ,sec
	

def handle_xp(total_xp , lvl , add=0,buyer = True):
	s = Settings.objects.all()[0]
	l = LevelManager.objects.all().order_by('-lvl')[0]
	print('test')
	print(l.lvl)
	new_lvl = lvl
	i = 0
	tot = total_xp
	while i < l.lvl:
		lm = LevelManager.objects.filter(lvl = i+1)[0]
		if buyer:
			needed = lm.buyer_xp
		else:
			needed = lm.seller_xp
		
		if total_xp >= needed:
			total_xp -= needed
			if total_xp == 0:
				total_xp = needed
				remaining = 0 
				break
		else:
			remaining = needed - total_xp
			break
		
		i += 1


	print(tot)
	if total_xp > 0 and LevelManager.objects.filter(lvl = i+1).count() == 0:
		remaining = 0
		total_xp = needed
	if (i+1) < lvl and remaining == 0 and (i+2) <= l.lvl:
		total_xp = 0
		lm = LevelManager.objects.filter(lvl = i+2)[0]
		if buyer:
			needed = lm.buyer_xp
		else:
			needed = lm.seller_xp
		remaining = needed
	
	print(tot)
		
	if add >= remaining and (new_lvl +1) <= l.lvl:
		tot += remaining
		new_lvl += 1
	elif (new_lvl + 1) > l.lvl and add >= remaining:
		tot += remaining
	else:
		print('adding here ?')
		tot  += add
	
	return tot,lvl,remaining,total_xp,new_lvl
		
def get_price(s='BTC-EUR'):
	#s = Settings.objects.all()[0]
	return xmr_rate

def calculate_fee(price,lvl, buyer=True):
	l = LevelManager.objects.filter(lvl = lvl)[0]
	if buyer:
		return round(price * (l.buyer_trade_fee / 100) , 8)
	else:
		return round(price * (l.seller_trade_fee / 100) , 8)


def get_avg_rating(u):
	os = Order.objects.filter(seller = u )
		
	avg_score = 0
	count = 0
	for o in os:
		
				

		if o.seller_reviewed:
			avg_score += o.seller_score
			count += 1
	if count == 0:
		pass
	else:
		avg_score = round(avg_score / count , 2)
		

		
	return  avg_score 

def get_sel_orders(u):
	return Order.objects.filter(seller = u ).count()

def get_earned(u):
	
	earned = 0	
	os = Order.objects.filter(seller = u )
	for o in os:
		if o.completed:
			obj = o.orderdetails_set.all()[0]
			earned += round((obj.price * obj.quantity ) - obj.fee , 2)

	earned = round(earned,2)

	return earned
	