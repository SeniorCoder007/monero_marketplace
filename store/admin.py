from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(CustomUser)
admin.site.register(ProductCat)
admin.site.register(Accounts)
admin.site.register(Cart)
admin.site.register(Transactions)
admin.site.register(Order)
#admin.site.register(OrderHolder)
admin.site.register(OrderDetails)
admin.site.register(Delivery)
admin.site.register(Category)
admin.site.register(Dispute)
#admin.site.register(DisputedOrders)
admin.site.register(Messages)
#admin.site.register(BuyerReview)
admin.site.register(Notifications)
admin.site.register(Receivers)
admin.site.register(Auction)
admin.site.register(bids)