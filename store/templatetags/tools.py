from django import template
import requests as req
import base64
from store.models import *


register = template.Library()
@register.simple_tag
def get_elem(dt,key):
	try:
		print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
		return dt[key]
	except Exception as e:
		print(str(e))


@register.simple_tag
def define(x):
	return x

@register.simple_tag
def increment(x):
	return x + 1



@register.simple_tag
def render_dec(number=None):
	print(number)
	pr = '{:.20f}'.format(float(number))
	if 'e' in str(float(number)):
		st_num = str(number)[1:]
		#print(st_num)
		indice = st_num.find('-')
		#print(indice)
		length1 = int(st_num[indice+1:])
		#print(length1)
		indice  = pr.find('.')
		final_num = pr[:length1+indice+1]
		return final_num
	else:
		return str(number)


@register.simple_tag
def get_qrcode(data=None):
	if data:
		resp = req.get('http://chart.apis.google.com/chart?cht=qr&chs=230x230&chl={0}'.format(data))
		encoded = base64.b64encode(resp.content)
		return 'data:image/png;base64,'+ encoded.decode()

	else:
		return ''

@register.simple_tag
def format_date(d , model = False):
	
	print(d)
	print(type(d))
	if model:
		return d.date.strftime('%a %H:%M  %d/%m/%y')
	return d.strftime('%a %H:%M  %d/%m/%y')

@register.simple_tag
def get_holder_childs(h):
	return h.orderdetails_set.all()

@register.simple_tag
def get_orderdetails_childs(h):
	return h.delivery_set.all()
	
@register.simple_tag
def get_l(lst,i):
	return lst[i]

@register.simple_tag
def add_to(x,y,coeff=1):
	return x + y*coeff



@register.simple_tag
def get_order(d):
	print('issueeeee here')
	return d.order

@register.simple_tag
def get_title(d):
	title = []
	detail = d.order.orderdetails_set.all()[0]
	return detail.prodname

@register.simple_tag
def get_order_title(o):
	
	return o.orderdetails_set.all()[0].prodname
	

@register.simple_tag
def get_price(d):
	price = 0
	for do in d.dorders.all():
		price += do.order_details.price * do.order_details.quantity
	return price

@register.simple_tag
def get_seller(username):
	return CustomUser.objects.filter(username = username)[0]


@register.simple_tag
def get_int(x):
	return int(x)

@register.simple_tag
def get_avg_rating(u):
	os = Order.objects.filter(seller = u )
		
	avg_score = 0
	count = 0
	for o in os:
		
				

		if o.seller_reviewed:
			avg_score += o.seller_score
			count += 1
	if count == 0:
		pass
	else:
		avg_score = round(avg_score / count , 2)
		

		
	return  avg_score 

@register.simple_tag
def get_avg_user(username):
	u = CustomUser.objects.filter(username = username)[0]
	os = Order.objects.filter(seller = u )
		
	avg_score = 0
	count = 0
	for o in os:
		
				

		if o.seller_reviewed:
			avg_score += o.seller_score
			count += 1
	if count == 0:
		pass
	else:
		avg_score = round(avg_score / count , 2)
		

		
	return  avg_score 


@register.simple_tag
def get_earned(u):
	
	earned = 0	
	os = Order.objects.filter(seller = u )
	for o in os:
		if o.completed:
			obj = o.orderdetails_set.all()[0]
			earned += round((obj.price * obj.quantity ) - obj.fee , 2)

	earned = round(earned,2)

	return earned

@register.simple_tag
def format_dec_num(n):
	return "{:.2f}".format(n)

@register.simple_tag
def get_sel_orders(u):
	return Order.objects.filter(seller = u ).count()


@register.simple_tag
def get_replace(d):
	print('here')
	return d.order.orderdetails_set.all()[0].replace_time

@register.simple_tag
def get_replace_order(o):
	return o.orderdetails_set.all()[0].replace_time

@register.simple_tag
def get_range(n):
	return range(int(n))


@register.simple_tag
def get_rev_range(n):
	return range(5-int(n))

@register.simple_tag
def get_len(d):
	return len(d)